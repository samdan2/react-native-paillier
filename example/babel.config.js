const path = require('path');
const pak = require('../package.json');

module.exports = {
  presets: [
    [
      'module:metro-react-native-babel-preset',
      // this is the line need to be added
      { unstable_transformProfile: 'hermes-stable' },
    ],
  ],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.tsx', '.ts', '.js', '.json'],
        alias: {
          [pak.name]: path.join(__dirname, '..', pak.source),
        },
      },
    ],
  ],
};
