const path = require('path');

module.exports = {
  dependencies: {
    'react-native-paillier': {
      root: path.join(__dirname, '..'),
    },
  },
};
