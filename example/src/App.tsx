import * as React from 'react';

import { StyleSheet, Text, View } from 'react-native';
import { generateRandomKeys } from 'react-native-paillier';

export default function App() {
  const [loading, setLoading] = React.useState<boolean>(false);

  React.useEffect(() => {
    const initFn = async () => {
      try {
        setLoading(true);
        let time1 = new Date();
        const { publicKey, privateKey } = await generateRandomKeys(2048, true);
        let time2 = new Date();
        console.log(
          'generateRandomKeys time(ms)',
          time2.getTime() - time1.getTime()
        );
        const m1 = 12345678901234567890n;
        const m2 = 5n;

        // encryption/decryption
        time1 = new Date();
        const c1 = publicKey.encrypt(m1);
        time2 = new Date();
        console.log('encrypt time(ms)', time2.getTime() - time1.getTime());
        time1 = new Date();
        console.log(privateKey.decrypt(c1)); // 12345678901234567890n
        time2 = new Date();
        console.log('decrypt time(ms)', time2.getTime() - time1.getTime());
        console.log(
          'encrypt decrypt time(ms)',
          time2.getTime() - time1.getTime()
        );

        // homomorphic addition of two ciphertexts (encrypted numbers)
        const c2 = publicKey.encrypt(m2);
        const encryptedSum = publicKey.addition(c1, c2);
        console.log(privateKey.decrypt(encryptedSum)); // m1 + m2 = 12345678901234567895n

        // multiplication by k
        const k = 10n;
        const encryptedMul = publicKey.multiply(c1, k);
        console.log(privateKey.decrypt(encryptedMul)); // k · m1 = 123456789
      } finally {
        setLoading(false);
      }
    };
    initFn();
  }, []);

  return (
    <View style={styles.container}>
      <Text>Loading state: {loading ? 'true' : 'false'}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
