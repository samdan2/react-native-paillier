package com.silencelaboratories.twopartyecdsa.phe;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;


public class KeyPairBuilder {

    private static final int bits = 2048;
    private static final int certainty = 100;

    public static KeyPair generateKeyPair() {
        Random rng = new SecureRandom();
        BigInteger p, q, n;
        int length = bits / 2;
        do {
            p = new BigInteger(length, certainty, rng);
            q = new BigInteger(length, certainty, rng);
            n = p.multiply(q);
        }
        while (p.equals(q) || n.bitLength() != bits);
        PaillierPublicKey paillierPublicKey = new PaillierPublicKey(n);
        PaillierPrivateKey paillierPrivateKey = new PaillierPrivateKey(paillierPublicKey, p, q);
        return new KeyPair(paillierPrivateKey, paillierPublicKey);
    }

}
