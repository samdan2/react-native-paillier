package com.silencelaboratories.twopartyecdsa;

import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.EncryptedNumberField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPublicKeyField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;

/**
 * Party2 KeyShare class as result of TSS(2,2) DKG
 */
public class P2KeyShare {

    private final BigInteger x2;
    private final ECPoint publicKey;
    private final EncryptedNumber cKeyX1;
    private final PaillierPublicKey paillierPublicKey;

    public P2KeyShare(
            BigInteger x2,
            ECPoint publicKey,
            EncryptedNumber cKeyX1,
            PaillierPublicKey paillierPublicKey) {
        this.x2 = x2;
        this.publicKey = publicKey;
        this.cKeyX1 = cKeyX1;
        this.paillierPublicKey = paillierPublicKey;
    }

    public BigInteger getX2() {
        return x2;
    }

    public ECPoint getPublicKey() {
        return publicKey;
    }

    public String getPublicKeyHex() {
        return ECPointField.toHex(publicKey);
    }

    public EncryptedNumber getcKeyX1() {
        return cKeyX1;
    }

    public PaillierPublicKey getPaillierPublicKey() {
        return paillierPublicKey;
    }

    /**
     * Serializer method for P2KeyShare
     *
     * @return JSONObject
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("x2", PosBigIntegerField.toHex(x2));
        m.put("public_key", ECPointField.toHex(publicKey));
        m.put("c_key_x1", EncryptedNumberField.serialize(cKeyX1));
        m.put("paillier_public_key", PaillierPublicKeyField.serialize(paillierPublicKey));
        return m;
    }

    /**
     * Deserializer method for P2KeyShare
     *
     * @param m JSONObject from toJSONObject() method
     * @return P2KeyShare object
     * @throws JSONException
     */
    public static P2KeyShare fromJSONObject(JSONObject m) throws JSONException {
        BigInteger x2 = PosBigIntegerField.fromHex(m.getString("x2"));
        ECPoint publicKey = ECPointField.fromHex(m.getString("public_key"));
        PaillierPublicKey paillierPublicKey = PaillierPublicKeyField.deserialize(
                m.getString("paillier_public_key")
        );
        EncryptedNumber cKeyX1 = EncryptedNumberField.deserialize(
                m.getString("c_key_x1"), paillierPublicKey
        );
        return new P2KeyShare(x2, publicKey, cKeyX1, paillierPublicKey);
    }

}
