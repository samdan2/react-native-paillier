package com.silencelaboratories.twopartyecdsa.signature;


import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_1;
import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_2;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomCurveScalar;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBelow;
import static com.silencelaboratories.twopartyecdsa.zkproofs.HashCommitment.verifyCommitment;

import com.silencelaboratories.twopartyecdsa.P2KeyShare;
import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.BytesBase64Field;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;
import com.silencelaboratories.twopartyecdsa.utils.Pair;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Objects;

/**
 * Party2 Signature class for TSS(2,2)
 */
public class P2Signature {

    enum Party2SignatureState {
        COMPLETE,
        FAILED,
        PROCESS_SIGN_MSG_1,
        PROCESS_SIGN_MSG_3,
        PROCESS_SIGN_MSG_5
    }

    private final ECPoint curveG = CurveSecp256k1.basePointG();
    private final BigInteger curveQ = CurveSecp256k1.order();

    private final String sessionId;
    private final byte[] messageHash;
    private final P2KeyShare p2KeyShare;
    private BigInteger k2;
    private String commitment;
    private Party2SignatureState state;

    /**
     * P2Signature constructor
     *
     * @param  sessionId Session ID for Signature action
     * @param  messageHash 32 bytes of message hash to sign
     * @param  p2KeyShare P2KeyShare object from DKG
     */
    public P2Signature(String sessionId, byte[] messageHash, P2KeyShare p2KeyShare) {
        this.sessionId = sessionId;
        this.messageHash = messageHash;
        this.p2KeyShare = p2KeyShare;
        state = Party2SignatureState.PROCESS_SIGN_MSG_1;
    }

    private P2Signature(
            String sessionId,
            byte[] messageHash,
            P2KeyShare p2KeyShare,
            BigInteger k2,
            String commitment,
            Party2SignatureState state) {
        this.sessionId = sessionId;
        this.messageHash = messageHash;
        this.p2KeyShare = p2KeyShare;
        this.k2 = k2;
        this.commitment = commitment;
        this.state = state;
    }

    /**
     * Serializer method for P2Signature
     *
     * @return JSONObject
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("session_id", sessionId);
        m.put("messageHash", BytesBase64Field.serialize(messageHash));
        m.put("p2KeyShare", p2KeyShare.toJSONObject());
        m.put("k2", JSONObject.NULL);
        m.put("commitment", JSONObject.NULL);
        m.put("state", state);
        if (k2 != null)
            m.put("k2", PosBigIntegerField.toBase64(k2));
        if (commitment != null)
            m.put("commitment", commitment);
        return m;
    }

    /**
     * Deserializer method for P2Signature
     *
     * @param m JSONObject from toJSONObject() method
     * @return P2Signature object
     * @throws JSONException
     */
    public static P2Signature fromJSONObject(JSONObject m) throws JSONException {
        String sessionId = m.getString("session_id");
        byte[] messageHash = BytesBase64Field.deserialize(
                m.getString("messageHash")
        );
        P2KeyShare p2KeyShare = P2KeyShare.fromJSONObject(
                m.getJSONObject("p2KeyShare")
        );
        String stateStr = m.getString("state");
        Party2SignatureState state;
        switch (stateStr) {
            case ("COMPLETE"):
                state = Party2SignatureState.COMPLETE;
                break;
            case ("PROCESS_SIGN_MSG_1"):
                state = Party2SignatureState.PROCESS_SIGN_MSG_1;
                break;
            case ("PROCESS_SIGN_MSG_3"):
                state = Party2SignatureState.PROCESS_SIGN_MSG_3;
                break;
            case ("PROCESS_SIGN_MSG_5"):
                state = Party2SignatureState.PROCESS_SIGN_MSG_5;
                break;
            default:
                state = Party2SignatureState.FAILED;
                break;
        }

        BigInteger k2 = null;
        String commitment = null;

        if (!m.isNull("k2"))
            k2 = PosBigIntegerField.fromBase64(m.getString("k2"));
        if (!m.isNull("commitment"))
            commitment = m.getString("commitment");

        return new P2Signature(
                sessionId, messageHash, p2KeyShare, k2, commitment, state
        );
    }

    public boolean isActive() {
        boolean cond1 = (state != Party2SignatureState.FAILED);
        boolean cond2 = (state != Party2SignatureState.COMPLETE);
        return cond1 && cond2;
    }

    private SignMessage2 processMessage1(SignMessage1 signMsg1) throws SignFailed {
        if (state != Party2SignatureState.PROCESS_SIGN_MSG_1) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid state");
        }
        if (!sessionId.equals(signMsg1.getSessionId())) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid sessionId");
        }

        commitment = signMsg1.getCommitment();
        k2 = randomCurveScalar();
        ECPoint r2 = curveG.multiply(k2);
        DLOGProof dLogProof = DLOGProof.prove(k2, r2, sessionId, PARTY_ID_2);

        SignMessage2 signMsg2 = new SignMessage2(sessionId, r2, dLogProof);
        state = Party2SignatureState.PROCESS_SIGN_MSG_3;
        return signMsg2;
    }

    private SignMessage4 processMessage3(SignMessage3 signMsg3) throws SignFailed {
        if (state != Party2SignatureState.PROCESS_SIGN_MSG_3) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid state");
        }
        if (!sessionId.equals(signMsg3.getSessionId())) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid sessionId");
        }

        ECPoint r1 = signMsg3.getR1();
        DLOGProof dlogProof = signMsg3.getdLogProof();
        boolean cond1 = dlogProof.verify(r1, sessionId, PARTY_ID_1);
        if (!cond1) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid dLogProof");
        }
        BigInteger blindFactor = signMsg3.getBlindFactor();
        boolean cond2 = verifyCommitment(
                commitment, r1, dlogProof, blindFactor, sessionId, PARTY_ID_1
        );
        if (!cond2) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid Commitment");
        }
        PaillierPublicKey paillierPublicKey = p2KeyShare.getPaillierPublicKey();
        EncryptedNumber cKeyX1 = p2KeyShare.getcKeyX1();
        ECPoint rUpper = r1.multiply(k2);
        BigInteger r = rUpper.normalize().getXCoord().toBigInteger().mod(curveQ);
        BigInteger m = PosBigIntegerField.fromBytes(messageHash);
        BigInteger ro = randomSampleBelow(curveQ.pow(2));
        BigInteger k2Inv = k2.modInverse(curveQ);
        EncryptedNumber c1 = paillierPublicKey.encrypt(
                ro.multiply(curveQ).add(k2Inv.multiply(m).mod(curveQ))
        );
        BigInteger v = k2Inv.multiply(r).multiply(p2KeyShare.getX2());
        EncryptedNumber c2 = cKeyX1.mul(v);
        EncryptedNumber c3 = c1.add(c2);

        SignMessage4 signMsg4 = new SignMessage4(sessionId, c3);
        state = Party2SignatureState.PROCESS_SIGN_MSG_5;
        return signMsg4;
    }

    private String processMessage5(SignMessage5 signMsg5) throws SignFailed {
        if (state != Party2SignatureState.PROCESS_SIGN_MSG_5) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid state");
        }
        if (!sessionId.equals(signMsg5.getSessionId())) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid sessionId");
        }

        String signature = signMsg5.getSignature();
        boolean signatureCorrect = CurveSecp256k1.verifySignature(
                messageHash, p2KeyShare.getPublicKey(), signature
        );
        if (!signatureCorrect) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed("Invalid signature");
        }

        state = Party2SignatureState.COMPLETE;
        return signature;
    }

    /**
     * Processes messages of Signature protocol
     *
     * @param  message JSONObject
     * @return pair of the message object to send and
     * hex signature after the Signature is complete
     * @throws SignFailed
     */
    public Pair<JSONObject, String> processMessage(JSONObject message)
            throws SignFailed {
        if (!this.isActive()) {
            throw new SignFailed("Signature was already Completed or Failed");
        }

        String sessionId = message.getString("session_id");
        if (!Objects.equals(this.sessionId, sessionId))
            throw new SignFailed("Invalid sessionId");

        try {
            if (state == Party2SignatureState.PROCESS_SIGN_MSG_1) {
                SignMessage1 signMsg1 = SignMessage1.fromJSONObject(message);
                SignMessage2 signMsg2 = processMessage1(signMsg1);
                return new Pair<>(signMsg2.toJSONObject(), null);
            }
            if (state == Party2SignatureState.PROCESS_SIGN_MSG_3) {
                SignMessage3 signMsg3 = SignMessage3.fromJSONObject(message);
                SignMessage4 signMsg4 = processMessage3(signMsg3);
                return new Pair<>(signMsg4.toJSONObject(), null);
            }
            if (state == Party2SignatureState.PROCESS_SIGN_MSG_5) {
                SignMessage5 signMsg5 = SignMessage5.fromJSONObject(message);
                String signature = processMessage5(signMsg5);
                return new Pair<>(null, signature);
            }

        } catch (Exception e) {
            state = Party2SignatureState.FAILED;
            throw new SignFailed(e);
        }
        state = Party2SignatureState.FAILED;
        throw new SignFailed("");
    }

}
