package com.silencelaboratories.twopartyecdsa.serializers;

import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;

public class EncryptedNumberField {

    public static byte[] toBytes(EncryptedNumber encryptedNumber) {
        return PosBigIntegerField.toBytes(encryptedNumber.getCiphertext());
    }

    public static String serialize(EncryptedNumber data) {
        return BytesBase64Field.serialize(EncryptedNumberField.toBytes(data));
    }

    public static EncryptedNumber deserialize(String data, PaillierPublicKey paillierPublicKey) {
        return new EncryptedNumber(paillierPublicKey, PosBigIntegerField.fromBase64(data));

    }

}
