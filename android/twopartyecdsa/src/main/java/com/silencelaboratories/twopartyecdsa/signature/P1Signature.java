package com.silencelaboratories.twopartyecdsa.signature;

import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_1;
import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_2;
import static com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1.signatureToHex;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomCurveScalar;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBitLength;
import static com.silencelaboratories.twopartyecdsa.zkproofs.HashCommitment.createCommitment;

import com.silencelaboratories.twopartyecdsa.P1KeyShare;
import com.silencelaboratories.twopartyecdsa.keygen.KeyGenFailed;
import com.silencelaboratories.twopartyecdsa.keygen.P1KeyGen;
import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierException;
import com.silencelaboratories.twopartyecdsa.serializers.BytesBase64Field;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;
import com.silencelaboratories.twopartyecdsa.utils.Pair;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Objects;

/**
 * Party1 Signature class for TSS(2,2)
 */
public class P1Signature {

    enum Party1SignatureState {
        COMPLETE,
        FAILED,
        GET_SIGN_MSG_1,
        PROCESS_SIGN_MSG_2,
        PROCESS_SIGN_MSG_4
    }

    private final ECPoint curveG = CurveSecp256k1.basePointG();
    private final BigInteger curveQ = CurveSecp256k1.order();

    private final String sessionId;
    private final byte[] messageHash;
    private final P1KeyShare p1KeyShare;
    private BigInteger k1;
    private ECPoint r1;
    private DLOGProof dLogProof;
    private BigInteger blindFactor;
    private BigInteger r;
    private Party1SignatureState state;

    /**
     * P1Signature constructor
     *
     * @param  sessionId Session ID for Signature action
     * @param  messageHash 32 bytes of message hash to sign
     * @param  p1KeyShare P1KeyShare object from DKG
     */
    public P1Signature(String sessionId, byte[] messageHash, P1KeyShare p1KeyShare) {
        this.sessionId = sessionId;
        this.messageHash = messageHash;
        this.p1KeyShare = p1KeyShare;
        state = Party1SignatureState.GET_SIGN_MSG_1;
    }

    private P1Signature(
            String sessionId,
            byte[] messageHash,
            P1KeyShare p1KeyShare,
            BigInteger k1,
            ECPoint r1,
            DLOGProof dLogProof,
            BigInteger blindFactor,
            BigInteger r,
            Party1SignatureState state) {
        this.sessionId = sessionId;
        this.messageHash = messageHash;
        this.p1KeyShare = p1KeyShare;
        this.k1 = k1;
        this.r1 = r1;
        this.dLogProof = dLogProof;
        this.blindFactor = blindFactor;
        this.r = r;
        this.state = state;
    }

    /**
     * Serializer method for P1Signature
     *
     * @return JSONObject
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("session_id", sessionId);
        m.put("messageHash", BytesBase64Field.serialize(messageHash));
        m.put("p1KeyShare", p1KeyShare.toJSONObject());
        m.put("k1", JSONObject.NULL);
        m.put("r1", JSONObject.NULL);
        m.put("dLogProof", JSONObject.NULL);
        m.put("blindFactor", JSONObject.NULL);
        m.put("r", JSONObject.NULL);
        m.put("state", state);
        if (k1 != null)
            m.put("k1", PosBigIntegerField.toBase64(k1));
        if (r1 != null)
            m.put("r1", ECPointField.serialize(r1));
        if (dLogProof != null)
            m.put("dLogProof", dLogProof.toJSONObject());
        if (blindFactor != null)
            m.put("blindFactor", PosBigIntegerField.toBase64(blindFactor));
        if (r != null)
            m.put("r", PosBigIntegerField.toBase64(r));
        return m;
    }

    /**
     * Deserializer method for P1Signature
     *
     * @param m JSONObject from toJSONObject() method
     * @return P1Signature object
     * @throws JSONException
     */
    public static P1Signature fromJSONObject(JSONObject m) throws JSONException {
        String sessionId = m.getString("session_id");
        byte[] messageHash = BytesBase64Field.deserialize(
                m.getString("messageHash")
        );
        P1KeyShare p1KeyShare = P1KeyShare.fromJSONObject(
                m.getJSONObject("p1KeyShare")
        );
        String stateStr = m.getString("state");
        Party1SignatureState state;
        switch (stateStr) {
            case ("COMPLETE"):
                state = Party1SignatureState.COMPLETE;
                break;
            case ("GET_SIGN_MSG_1"):
                state = Party1SignatureState.GET_SIGN_MSG_1;
                break;
            case ("PROCESS_SIGN_MSG_2"):
                state = Party1SignatureState.PROCESS_SIGN_MSG_2;
                break;
            case ("PROCESS_SIGN_MSG_4"):
                state = Party1SignatureState.PROCESS_SIGN_MSG_4;
                break;
            default:
                state = Party1SignatureState.FAILED;
                break;
        }

        BigInteger k1 = null;
        ECPoint r1 = null;
        DLOGProof dLogProof = null;
        BigInteger blindFactor = null;
        BigInteger r = null;

        if (!m.isNull("k1"))
            k1 = PosBigIntegerField.fromBase64(m.getString("k1"));
        if (!m.isNull("r1"))
            r1 = ECPointField.deserialize(m.getString("r1"));
        if (!m.isNull("dLogProof"))
            dLogProof = DLOGProof.fromJSONObject(m.getJSONObject("dLogProof"));
        if (!m.isNull("blindFactor"))
            blindFactor = PosBigIntegerField.fromBase64(m.getString("blindFactor"));
        if (!m.isNull("r"))
            r = PosBigIntegerField.fromBase64(m.getString("r"));

        return new P1Signature(
                sessionId, messageHash, p1KeyShare, k1, r1, dLogProof, blindFactor, r, state
        );
    }

    public boolean isActive() {
        boolean cond1 = (state != Party1SignatureState.FAILED);
        boolean cond2 = (state != Party1SignatureState.COMPLETE);
        return cond1 && cond2;
    }

    private SignMessage1 getSignMessage1() throws SignFailed {
        if (state != Party1SignatureState.GET_SIGN_MSG_1) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid state");
        }

        k1 = randomCurveScalar();
        r1 = curveG.multiply(k1);
        dLogProof = DLOGProof.prove(k1, r1, sessionId, PARTY_ID_1);
        blindFactor = randomSampleBitLength(256);
        String commitment = createCommitment(r1, dLogProof, blindFactor, sessionId, PARTY_ID_1);

        SignMessage1 signMessage1 = new SignMessage1(sessionId, commitment);
        state = Party1SignatureState.PROCESS_SIGN_MSG_2;
        return signMessage1;
    }

    /**
     * Gets first message for Signature protocol
     *
     * @return JSONObject of serialized KeyGenMessage1
     * @throws SignFailed
     */
    public JSONObject getMessage1() throws SignFailed {
        return getSignMessage1().toJSONObject();
    }

    private SignMessage3 processMessage2(SignMessage2 signMsg2) throws SignFailed {
        if (state != Party1SignatureState.PROCESS_SIGN_MSG_2) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid state");
        }
        if (!sessionId.equals(signMsg2.getSessionId())) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid sessionId");
        }

        ECPoint r2 = signMsg2.getR2();
        DLOGProof dlogProof = signMsg2.getdLogProof();
        boolean cond1 = dlogProof.verify(r2, sessionId, PARTY_ID_2);
        if (!cond1) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid dLogProof");
        }
        ECPoint rUpper = r2.multiply(k1);
        r = rUpper.normalize().getXCoord().toBigInteger().mod(curveQ);

        SignMessage3 signMsg3 = new SignMessage3(sessionId, r1, this.dLogProof, blindFactor);
        state = Party1SignatureState.PROCESS_SIGN_MSG_4;
        return signMsg3;
    }

    private Pair<SignMessage5, String> processMessage4(SignMessage4 signMsg4)
            throws SignFailed {
        if (state != Party1SignatureState.PROCESS_SIGN_MSG_4) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid state");
        }
        if (!sessionId.equals(signMsg4.getSessionId())) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid sessionId");
        }

        EncryptedNumber c3 = signMsg4.getC3();
        BigInteger s1;
        try {
            s1 = p1KeyShare.getPaillierPrivateKey().decrypt(c3);
        } catch (PaillierException e) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid Paillier decrypt");
        }
        BigInteger k1Inv = k1.modInverse(curveQ);
        BigInteger s2 = k1Inv.multiply(s1).mod(curveQ);
        BigInteger s;
        if (s2.compareTo(curveQ.subtract(s2)) < 0) {
            s = s2;
        } else {
            s = curveQ.subtract(s2);
        }
        String signature = signatureToHex(r, s);

        boolean signatureCorrect = CurveSecp256k1.verifySignature(
                messageHash, p1KeyShare.getPublicKey(), signature
        );
        if (!signatureCorrect) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed("Invalid signature");
        }

        SignMessage5 signMsg5 = new SignMessage5(sessionId, signature);

        state = Party1SignatureState.COMPLETE;
        return new Pair<>(signMsg5, signature);
    }

    /**
     * Processes messages of Signature protocol
     *
     * @param  message JSONObject
     * @return pair of the message object to send and
     * hex signature after the Signature is complete
     * @throws SignFailed
     */
    public Pair<JSONObject, String> processMessage(JSONObject message)
            throws SignFailed {
        if (!this.isActive()) {
            throw new SignFailed("Signature was already Completed or Failed");
        }

        String sessionId = message.getString("session_id");
        if (!Objects.equals(this.sessionId, sessionId))
            throw new SignFailed("Invalid sessionId");

        try {
            if (state == Party1SignatureState.PROCESS_SIGN_MSG_2) {
                SignMessage2 signMsg2 = SignMessage2.fromJSONObject(message);
                SignMessage3 signMsg3 = processMessage2(signMsg2);
                return new Pair<>(signMsg3.toJSONObject(), null);
            }
            if (state == Party1SignatureState.PROCESS_SIGN_MSG_4) {
                SignMessage4 signMsg4 = SignMessage4.fromJSONObject(
                        message, p1KeyShare.getPaillierPublicKey()
                );
                Pair<SignMessage5, String> pair = processMessage4(signMsg4);
                SignMessage5 signMsg5 = pair.getFirst();
                String signature = pair.getSecond();
                return new Pair<>(signMsg5.toJSONObject(), signature);
            }

        } catch (Exception e) {
            state = Party1SignatureState.FAILED;
            throw new SignFailed(e);
        }
        state = Party1SignatureState.FAILED;
        throw new SignFailed("");
    }

}
