package com.silencelaboratories.twopartyecdsa.serializers;


import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;

import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;

public class ECPointField {

    public static byte[] toBytes(ECPoint p) {
        ECPoint point = p.normalize();
        byte[] x = PosBigIntegerField.toBytes(point.getXCoord().toBigInteger(), 32);
        byte[] y = PosBigIntegerField.toBytes(point.getYCoord().toBigInteger(), 32);
        byte[] result = new byte[64];
        System.arraycopy(x, 0, result, 0, x.length);
        System.arraycopy(y, 0, result, x.length, y.length);
        return result;
    }

    public static ECPoint fromBytes(byte[] data) {
        if (data.length != 64) {
            throw new IllegalArgumentException("Bytes to ECPoint error, length != 64 bytes");
        }
        byte[] x = new byte[32];
        byte[] y = new byte[32];
        System.arraycopy(data, 0, x, 0, 32);
        System.arraycopy(data, 32, y, 0, 32);
        BigInteger xCoord = PosBigIntegerField.fromBytes(x);
        BigInteger yCoord = PosBigIntegerField.fromBytes(y);
        ECCurve curve = CurveSecp256k1.curve();
        return curve.createPoint(xCoord, yCoord);
    }

    public static String toHex(ECPoint data) {
        return BytesHexField.serialize(ECPointField.toBytes(data));
    }

    public static ECPoint fromHex(String data) {
        return ECPointField.fromBytes(BytesHexField.deserialize(data));
    }

    public static String serialize(ECPoint data) {
        return BytesBase64Field.serialize(ECPointField.toBytes(data));
    }

    public static ECPoint deserialize(String data) {
        return ECPointField.fromBytes(BytesBase64Field.deserialize(data));
    }

}
