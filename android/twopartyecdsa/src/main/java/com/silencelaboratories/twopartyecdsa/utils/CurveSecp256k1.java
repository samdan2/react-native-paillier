package com.silencelaboratories.twopartyecdsa.utils;


import com.silencelaboratories.twopartyecdsa.serializers.BytesHexField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.StringField;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public final class CurveSecp256k1 {

    public static ECParameterSpec getCurveParams() {
        return ECNamedCurveTable.getParameterSpec("secp256k1");
    }

    public static ECCurve curve() {
        return getCurveParams().getCurve();
    }

    public static ECPoint basePointG() {
        return getCurveParams().getG();
    }

    public static BigInteger order() {
        return getCurveParams().getN();
    }


    public static byte[] sha256Hash(String message) {
        byte[] messageBytes = StringField.toBytes(message);
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(messageBytes);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

    }

    public static String signatureToHex(BigInteger r, BigInteger s) {
        byte[] rBytes = PosBigIntegerField.toBytes(r, 32);
        byte[] sBytes = PosBigIntegerField.toBytes(s, 32);
        String signature = BytesHexField.serialize(rBytes);
        signature += BytesHexField.serialize(sBytes);
        return signature;
    }

    public static Pair<BigInteger, BigInteger> hexSignatureToPairRS(String signature) {
        byte[] b = BytesHexField.deserialize(signature);
        if (b.length != 64) {
            throw new IllegalArgumentException("Bytes to ECPoint error, length != 64 bytes");
        }
        byte[] r = new byte[32];
        byte[] s = new byte[32];
        System.arraycopy(b, 0, r, 0, 32);
        System.arraycopy(b, 32, s, 0, 32);
        return new Pair<>(PosBigIntegerField.fromBytes(r), PosBigIntegerField.fromBytes(s));
    }

    public static boolean verifySignature(
            byte[] messageHash, ECPoint publicKey, String signatureHex) {
        Pair<BigInteger, BigInteger> pair = hexSignatureToPairRS(signatureHex);
        BigInteger r = pair.getFirst();
        BigInteger s = pair.getSecond();
        BigInteger m = PosBigIntegerField.fromBytes(messageHash);

        ECPoint g = CurveSecp256k1.basePointG();
        BigInteger n = CurveSecp256k1.order();

        BigInteger nMinusOne = n.subtract(BigInteger.ONE);
        if ((r.compareTo(BigInteger.ONE) < 0) || (r.compareTo(nMinusOne) > 0)) {
            return false;
        }
        if ((s.compareTo(BigInteger.ONE) < 0) || (s.compareTo(nMinusOne) > 0)) {
            return false;
        }
        BigInteger c = s.modInverse(n);
        BigInteger u1 = m.multiply(c).mod(n);
        BigInteger u2 = r.multiply(c).mod(n);
        ECPoint xy = g.multiply(u1).add(publicKey.multiply(u2));
        BigInteger v = xy.normalize().getXCoord().toBigInteger().mod(n);
        return v.equals(r);
    }

}
