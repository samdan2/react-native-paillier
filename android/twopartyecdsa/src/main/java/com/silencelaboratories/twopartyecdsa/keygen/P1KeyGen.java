package com.silencelaboratories.twopartyecdsa.keygen;


import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_1;
import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_2;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomCurveScalar;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBelow;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBitLength;
import static com.silencelaboratories.twopartyecdsa.zkproofs.HashCommitment.createCommitment;

import com.silencelaboratories.twopartyecdsa.P1KeyShare;
import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.KeyPair;
import com.silencelaboratories.twopartyecdsa.phe.KeyPairBuilder;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPrivateKey;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPrivateKeyField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPublicKeyField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;
import com.silencelaboratories.twopartyecdsa.utils.Pair;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.NiCorrectKeyProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLwSlackProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLwSlackStatement;
import com.silencelaboratories.twopartyecdsa.zkproofs.wiDLogProof.CompositeDLogProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Objects;

import jdk.internal.net.http.common.Log;

/**
 * Party1 DKG class for TSS(2,2)
 */
public class P1KeyGen {

    enum Party1KeyGenState {
        COMPLETE,
        FAILED,
        NOT_INITIALIZED,
        CREATE_KEY_GEN_MSG_1,
        PROCESS_KEY_GEN_MSG_2,
    }

    private final ECPoint curveG = CurveSecp256k1.basePointG();
    private final BigInteger curveQ = CurveSecp256k1.order();

    private final String sessionId;
    private BigInteger x1;
    private PaillierPublicKey paillierPublicKey;
    private PaillierPrivateKey paillierPrivateKey;
    private ECPoint q1;
    private DLOGProof dLogProof1;
    private BigInteger blindFactor1;
    private BigInteger eph1;
    private ECPoint e1;
    private DLOGProof dLogProof2;
    private BigInteger blindFactor2;
    private String expectedPublicKey;
    private Party1KeyGenState state;

    /**
     * P1KeyGen constructor
     *
     * @param  sessionId Session ID for DKG action
     * @param  x1 Secret bytes of Party1, private_key = (x1 + x2) mod q
     * @param  expectedPublicKey hex string of public_key, for key recovery and refresh purposes
     * @throws KeyGenFailed
     */
    public P1KeyGen(String sessionId, byte[] x1, String expectedPublicKey) throws KeyGenFailed {
        this.sessionId = sessionId;
        this.expectedPublicKey = expectedPublicKey;
        if (x1 != null) {
            if (x1.length != 32) {
                throw new KeyGenFailed("Invalid length of x2");
            }
            this.x1 = PosBigIntegerField.fromBytes(x1).mod(curveQ);
        }
        else {
            this.x1 = randomCurveScalar();
        }
        state = Party1KeyGenState.NOT_INITIALIZED;
    }

    /**
     * P1KeyGen constructor
     *
     * @param  sessionId Session ID for DKG action
     * @param  x1 Secret bytes of Party1, private_key = (x1 + x2) mod q
     * @throws KeyGenFailed
     */
    public P1KeyGen(String sessionId, byte[] x1) throws KeyGenFailed {
        this(sessionId, x1, null);
    }

    /**
     * P1KeyGen constructor
     * <p>
     * Initializes P1KeyGen instance with random x1
     *
     * @param  sessionId Session ID for DKG action
     * @return instance of P1KeyGen class
     * @throws KeyGenFailed
     */
    public P1KeyGen(String sessionId) throws KeyGenFailed {
        this(sessionId, null, null);
    }

    private P1KeyGen(
            String sessionId,
            BigInteger x1,
            PaillierPublicKey paillierPublicKey,
            PaillierPrivateKey paillierPrivateKey,
            ECPoint q1,
            DLOGProof dLogProof1,
            BigInteger blindFactor1,
            BigInteger eph1,
            ECPoint e1,
            DLOGProof dLogProof2,
            BigInteger blindFactor2,
            String expectedPublicKey,
            Party1KeyGenState state) {
        this.sessionId = sessionId;
        this.x1 = x1;
        this.paillierPublicKey = paillierPublicKey;
        this.paillierPrivateKey = paillierPrivateKey;
        this.q1 = q1;
        this.dLogProof1 = dLogProof1;
        this.blindFactor1 = blindFactor1;
        this.eph1 = eph1;
        this.e1 = e1;
        this.dLogProof2 = dLogProof2;
        this.blindFactor2 = blindFactor2;
        this.expectedPublicKey = expectedPublicKey;
        this.state = state;
    }

    /**
     * Serializer method for P1KeyGen
     *
     * @return JSONObject
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("session_id", sessionId);
        m.put("x1", PosBigIntegerField.toBase64(x1));
        m.put("paillierPublicKey", JSONObject.NULL);
        m.put("paillierPrivateKey", JSONObject.NULL);
        m.put("q1", JSONObject.NULL);
        m.put("dLogProof1", JSONObject.NULL);
        m.put("blindFactor1", JSONObject.NULL);
        m.put("eph1", JSONObject.NULL);
        m.put("e1", JSONObject.NULL);
        m.put("dLogProof2", JSONObject.NULL);
        m.put("blindFactor2", JSONObject.NULL);
        m.put("expectedPublicKey", JSONObject.NULL);
        m.put("state", state.toString());

        if (paillierPublicKey != null)
            m.put("paillierPublicKey", PaillierPublicKeyField.serialize(paillierPublicKey));
        if (paillierPrivateKey != null)
            m.put("paillierPrivateKey", PaillierPrivateKeyField.serialize(paillierPrivateKey));
        if (q1 != null)
            m.put("q1", ECPointField.serialize(q1));
        if (dLogProof1 != null)
            m.put("dLogProof1", dLogProof1.toJSONObject());
        if (blindFactor1 != null)
            m.put("blindFactor1", PosBigIntegerField.toBase64(blindFactor1));
        if (eph1 != null)
            m.put("eph1", PosBigIntegerField.toBase64(eph1));
        if (e1 != null)
            m.put("e1", ECPointField.serialize(e1));
        if (dLogProof2 != null)
            m.put("dLogProof2", dLogProof2.toJSONObject());
        if (blindFactor2 != null)
            m.put("blindFactor2", PosBigIntegerField.toBase64(blindFactor2));
        if (expectedPublicKey != null)
            m.put("expectedPublicKey", expectedPublicKey);

        return m;
    }

    /**
     * Deserializer method for P1KeyGen
     *
     * @param m JSONObject from toJSONObject() method
     * @return P1KeyGen object
     * @throws JSONException
     */
    public static P1KeyGen fromJSONObject(JSONObject m) throws JSONException {
        String sessionId = m.getString("session_id");
        BigInteger x1 = PosBigIntegerField.fromBase64(m.getString("x1"));

        PaillierPublicKey paillierPublicKey = PaillierPublicKeyField.deserialize(
                m.getString("paillierPublicKey")
        );
        PaillierPrivateKey paillierPrivateKey = PaillierPrivateKeyField.deserialize(
                m.getJSONObject("paillierPrivateKey")
        );
        String stateStr = m.getString("state");
        Party1KeyGenState state;
        switch (stateStr) {
            case ("COMPLETE"):
                state = Party1KeyGenState.COMPLETE;
                break;
            case ("NOT_INITIALIZED"):
                state = Party1KeyGenState.NOT_INITIALIZED;
                break;
            case ("CREATE_KEY_GEN_MSG_1"):
                state = Party1KeyGenState.CREATE_KEY_GEN_MSG_1;
                break;
            case ("PROCESS_KEY_GEN_MSG_2"):
                state = Party1KeyGenState.PROCESS_KEY_GEN_MSG_2;
                break;
            default:
                state = Party1KeyGenState.FAILED;
                break;
        }

        ECPoint q1 = null;
        DLOGProof dLogProof1 = null;
        BigInteger blindFactor1 = null;
        BigInteger eph1 = null;
        ECPoint e1 = null;
        DLOGProof dLogProof2 = null;
        BigInteger blindFactor2 = null;
        String expectedPublicKey = null;

        if (!m.isNull("q1"))
            q1 = ECPointField.deserialize(m.getString("q1"));
        if (!m.isNull("dLogProof1"))
            dLogProof1 = DLOGProof.fromJSONObject(m.getJSONObject("dLogProof1"));
        if (!m.isNull("blindFactor1"))
            blindFactor1 = PosBigIntegerField.fromBase64(m.getString("blindFactor1"));
        if (!m.isNull("eph1"))
            eph1 = PosBigIntegerField.fromBase64(m.getString("eph1"));
        if (!m.isNull("e1"))
            e1 = ECPointField.deserialize(m.getString("e1"));
        if (!m.isNull("dLogProof2"))
            dLogProof2 = DLOGProof.fromJSONObject(m.getJSONObject("dLogProof2"));
        if (!m.isNull("blindFactor2"))
            blindFactor2 = PosBigIntegerField.fromBase64(m.getString("blindFactor2"));
        if (!m.isNull("expectedPublicKey"))
            expectedPublicKey = m.getString("expectedPublicKey");

        return new P1KeyGen(
                sessionId, x1, paillierPublicKey, paillierPrivateKey, q1, dLogProof1, blindFactor1,
                eph1, e1, dLogProof2, blindFactor2, expectedPublicKey, state
        );
    }

    /**
     * Initializes P1KeyGen for Key Refresh purpose
     *
     * @param  sessionId Session ID for DKG action
     * @param  p1KeyShare Existed P1KeyShare object to refresh
     * @return P1KeyGen object
     * @throws KeyGenFailed
     * @throws JSONException
     */
    public static P1KeyGen getInstanceForKeyRefresh(String sessionId, P1KeyShare p1KeyShare)
            throws KeyGenFailed, JSONException {
        byte[] x1 = PosBigIntegerField.toBytes(p1KeyShare.getX1());
        String expectedPublicKey = ECPointField.toHex(p1KeyShare.getPublicKey());
        return new P1KeyGen(sessionId, x1, expectedPublicKey);
    }

    /**
     * Generates Paillier KeyPair
     */
    public void init() {
        KeyPair paillierKeyPair = KeyPairBuilder.generateKeyPair();
        paillierPrivateKey = paillierKeyPair.getPrivateKey();
        paillierPublicKey = paillierKeyPair.getPublicKey();
        state = Party1KeyGenState.CREATE_KEY_GEN_MSG_1;
    }

    public boolean isActive() {
        boolean cond1 = (state != Party1KeyGenState.FAILED);
        boolean cond2 = (state != Party1KeyGenState.COMPLETE);
        boolean cond3 = (state != Party1KeyGenState.NOT_INITIALIZED);
        return cond1 && cond2 && cond3;
    }

    private KeyGenMessage1 getKeyGenMessage1() throws KeyGenFailed {
        if (state != Party1KeyGenState.CREATE_KEY_GEN_MSG_1) {
            state = Party1KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid state");
        }
        q1 = curveG.multiply(x1);
        dLogProof1 = DLOGProof.prove(x1, q1, sessionId, PARTY_ID_1);
        blindFactor1 = randomSampleBitLength(256);
        String commitment1 = createCommitment(q1, dLogProof1, blindFactor1, sessionId, PARTY_ID_1);

        eph1 = randomCurveScalar();
        e1 = curveG.multiply(eph1);
        dLogProof2 = DLOGProof.prove(eph1, e1, sessionId, PARTY_ID_1);
        blindFactor2 = randomSampleBitLength(256);
        String commitment2 = createCommitment(e1, dLogProof2, blindFactor2, sessionId, PARTY_ID_1);

        KeyGenMessage1 keyGenMessage1 = new KeyGenMessage1(
                sessionId, commitment1, commitment2
        );
        state = Party1KeyGenState.PROCESS_KEY_GEN_MSG_2;
        return keyGenMessage1;
    }

    /**
     * Gets first message for DKG protocol
     *
     * @return JSONObject of serialized KeyGenMessage1
     * @throws KeyGenFailed
     */
    public JSONObject getMessage1() throws KeyGenFailed {
        return getKeyGenMessage1().toJSONObject();
    }

    private Pair<KeyGenMessage3, P1KeyShare> processMessage2(KeyGenMessage2 keyGenMessage2)
            throws KeyGenFailed {
        if (state != Party1KeyGenState.PROCESS_KEY_GEN_MSG_2) {
            state = Party1KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid state");
        }
        if (!sessionId.equals(keyGenMessage2.getSessionId())) {
            state = Party1KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid sessionId");
        }

        ECPoint q2 = keyGenMessage2.getQ2();
        DLOGProof dLogProof1 = keyGenMessage2.getdLogProof1();
        boolean cond1 = dLogProof1.verify(q2, sessionId, PARTY_ID_2);
        if (!cond1) {
            state = Party1KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid dLogProof1");
        }

        ECPoint publicKey = q2.multiply(x1);
        if (expectedPublicKey != null) {
            if (!expectedPublicKey.equals(ECPointField.toHex(publicKey))) {
                state = Party1KeyGenState.FAILED;
                throw new KeyGenFailed("Invalid publicKey");
            }
        }

        ECPoint e2 = keyGenMessage2.getE2();
        DLOGProof dLogProof2 = keyGenMessage2.getdLogProof2();
        boolean cond2 = dLogProof2.verify(e2, sessionId, PARTY_ID_2);
        if (!cond2) {
            state = Party1KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid dLogProof2");
        }
        // rotate private share x1 to rotateValue1
        ECPoint ephPoint = e2.multiply(eph1);
        BigInteger rotateValue1 = ephPoint.normalize().getXCoord().toBigInteger();
        x1 = x1.multiply(rotateValue1).mod(curveQ);

        NiCorrectKeyProof niCorrectKeyProof = NiCorrectKeyProof.prove(
                paillierPrivateKey, sessionId,PARTY_ID_1
        );
        BigInteger randomness = randomSampleBelow(paillierPublicKey.getN());
        EncryptedNumber cKeyX1 = paillierPublicKey.encrypt(x1, randomness);

        PDLProof pdlProof = PDLProof.prove(
                x1, randomness, paillierPublicKey, cKeyX1, sessionId, PARTY_ID_1
        );
        PDLwSlackStatement pdLwSlackStatement = pdlProof.getPdLwSlackStatement();
        PDLwSlackProof pdLwSlackProof = pdlProof.getPdLwSlackProof();
        CompositeDLogProof compositeDLogProof = pdlProof.getCompositeDLogProof();

        KeyGenMessage3 keyGenMessage3 = new KeyGenMessage3(
                sessionId, q1, this.dLogProof1, blindFactor1, cKeyX1,
                paillierPublicKey, niCorrectKeyProof,
                pdLwSlackStatement, pdLwSlackProof, compositeDLogProof,
                e1, this.dLogProof2, blindFactor2
        );

        P1KeyShare p1KeyShare = new P1KeyShare(
                x1, publicKey, paillierPrivateKey, paillierPublicKey
        );

        state = Party1KeyGenState.COMPLETE;
        return new Pair<>(keyGenMessage3, p1KeyShare);
    }

    /**
     * Processes messages of DKG protocol
     *
     * @param  message JSONObject
     * @return Pair<JSONObject, P1KeyShare> returns the message object to send and
     * the P1KeyShare object after the DKG is complete
     * @throws KeyGenFailed
     */
    public Pair<JSONObject, P1KeyShare> processMessage(JSONObject message)
            throws KeyGenFailed {
        if (!this.isActive()) {
            throw new KeyGenFailed("KeyGen was already Completed or Failed or Not Initialized");
        }

        String sessionId = message.getString("session_id");
        if (!Objects.equals(this.sessionId, sessionId))
            throw new KeyGenFailed("Invalid sessionId");

        try {
            if (state == Party1KeyGenState.PROCESS_KEY_GEN_MSG_2) {
                KeyGenMessage2 keyGenMsg2 = KeyGenMessage2.fromJSONObject(message);
                Pair<KeyGenMessage3, P1KeyShare> pair = processMessage2(keyGenMsg2);
                return new Pair<>(pair.getFirst().toJSONObject(), pair.getSecond());
            }
        } catch (Exception e) {
            state = Party1KeyGenState.FAILED;
            throw new KeyGenFailed(e);
        }
        state = Party1KeyGenState.FAILED;
        throw new KeyGenFailed("");
    }

}
