package com.silencelaboratories.twopartyecdsa.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;


public final class Utils {

    public static BigInteger randomSampleBitLength(int bitLength) {
        // return random number in range [1, 2^bitLength - 1]
        Random randomSource = new SecureRandom();
        BigInteger randomNumber;
        do {
            randomNumber = new BigInteger(bitLength, randomSource);
        } while (randomNumber.compareTo(BigInteger.ZERO) == 0);
        return randomNumber;
    }

    public static BigInteger randomSampleBelow(BigInteger upperLimit) {
        // return random number in range [1, upperLimit - 1]
        Random randomSource = new SecureRandom();
        BigInteger randomNumber;
        do {
            randomNumber = new BigInteger(upperLimit.bitLength(), randomSource);
        } while ((randomNumber.compareTo(upperLimit) >= 0) ||
                (randomNumber.compareTo(BigInteger.ZERO) == 0));
        return randomNumber;
    }

    public static BigInteger randomCurveScalar() {
        return randomSampleBelow(CurveSecp256k1.order());
    }

}
