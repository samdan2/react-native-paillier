package com.silencelaboratories.twopartyecdsa.serializers;


import java.math.BigInteger;

public class PosBigIntegerField {

    public static byte[] toBytes(BigInteger data) {
        byte[] array = data.toByteArray();
        if (array[0] == 0) {
            byte[] tmp = new byte[array.length - 1];
            System.arraycopy(array, 1, tmp, 0, tmp.length);
            array = tmp;
        }
        return array;
    }
    public static byte[] toBytes(BigInteger data, int order) {
        byte[] out = PosBigIntegerField.toBytes(data);
        int outLen = out.length;
        if (order > outLen) {
            byte[] result = new byte[order];
            System.arraycopy(out, 0, result, order-outLen, outLen);
            out = result;
        }
        return out;
    }

    public static BigInteger fromBytes(byte[] data) {
        byte[] first = {0};
        byte[] result = new byte[data.length + 1];
        System.arraycopy(first, 0, result, 0, first.length);
        System.arraycopy(data, 0, result, first.length, data.length);
        return new BigInteger(result);
    }

    public static String toBase64(BigInteger data) {
        return BytesBase64Field.serialize(PosBigIntegerField.toBytes(data));
    }

    public static BigInteger fromBase64(String data) {
        return PosBigIntegerField.fromBytes(BytesBase64Field.deserialize(data));
    }

    public static String toHex(BigInteger data) {
        return BytesHexField.serialize(PosBigIntegerField.toBytes(data));
    }

    public static BigInteger fromHex(String data) {
        return PosBigIntegerField.fromBytes(BytesHexField.deserialize(data));
    }

}
