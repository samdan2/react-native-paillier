package com.silencelaboratories.twopartyecdsa.keygen;


import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;


public class KeyGenMessage2 {

    private final static String PHASE = "key_gen_message_2";
    private final String sessionId;
    private final ECPoint q2;
    private final DLOGProof dLogProof1;
    private final ECPoint e2;
    private final DLOGProof dLogProof2;

    public KeyGenMessage2(
            String sessionId, ECPoint q2, DLOGProof dLogProof1, ECPoint e2, DLOGProof dLogProof2
    ) {
        this.sessionId = sessionId;
        this.q2 = q2;
        this.dLogProof1 = dLogProof1;
        this.e2 = e2;
        this.dLogProof2 = dLogProof2;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ECPoint getQ2() {
        return q2;
    }

    public DLOGProof getdLogProof1() {
        return dLogProof1;
    }

    public ECPoint getE2() {
        return e2;
    }

    public DLOGProof getdLogProof2() {
        return dLogProof2;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("q2", ECPointField.serialize(q2));
        m.put("dlog_proof_1", dLogProof1.toJSONObject());
        m.put("e2", ECPointField.serialize(e2));
        m.put("dlog_proof_2", dLogProof2.toJSONObject());
        return m;
    }

    public static KeyGenMessage2 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(KeyGenMessage2.PHASE)) {
                throw new SerializerException("Invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            ECPoint q2 = ECPointField.deserialize(m.getString("q2"));
            DLOGProof dLogProof1 = DLOGProof.fromJSONObject(m.getJSONObject("dlog_proof_1"));
            ECPoint e2 = ECPointField.deserialize(m.getString("e2"));
            DLOGProof dLogProof2 = DLOGProof.fromJSONObject(m.getJSONObject("dlog_proof_2"));
            return new KeyGenMessage2(sessionId, q2, dLogProof1, e2, dLogProof2);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("KeyGenMessage2 fromJSONObject() error: " + e);
        }
    }

}
