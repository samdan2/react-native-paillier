package com.silencelaboratories.twopartyecdsa.zkproofs;

import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.BytesHexField;
import com.silencelaboratories.twopartyecdsa.serializers.StringField;

import org.bouncycastle.math.ec.ECPoint;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;


public final class HashCommitment {

    public static String createCommitment(
            ECPoint q, DLOGProof dlogProof, BigInteger blindFactor, String sid, String pid
    ) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(PosBigIntegerField.toBytes(q.normalize().getXCoord().toBigInteger()));
            outputStream.write(PosBigIntegerField.toBytes(
                    dlogProof.getT().normalize().getXCoord().toBigInteger())
            );
            outputStream.write(PosBigIntegerField.toBytes(dlogProof.getS()));
            outputStream.write(PosBigIntegerField.toBytes(blindFactor));
            outputStream.write(StringField.toBytes(sid));
            outputStream.write(StringField.toBytes(pid));
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(outputStream.toByteArray());
            return BytesHexField.serialize(hash);

        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean verifyCommitment(
            String commitment, ECPoint q, DLOGProof dlogProof, BigInteger blindFactor,
            String sid, String pid
    ) {
        String commitmentTest = createCommitment(q, dlogProof, blindFactor, sid, pid);
        return Objects.equals(commitment, commitmentTest);
    }

}
