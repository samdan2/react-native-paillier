package com.silencelaboratories.twopartyecdsa.phe;

import java.math.BigInteger;


public class EncryptedNumber {

    private final BigInteger ciphertext;
    private final PaillierPublicKey paillierPublicKey;

    public EncryptedNumber(PaillierPublicKey paillierPublicKey, BigInteger ciphertext) {
        this.ciphertext = ciphertext;
        this.paillierPublicKey = paillierPublicKey;
    }

    public PaillierPublicKey getPublicKey() {
        return paillierPublicKey;
    }

    public BigInteger getCiphertext() {
        return ciphertext;
    }

    public EncryptedNumber add(EncryptedNumber encryptedNumber) {
        BigInteger encryptedSum = ciphertext.multiply(encryptedNumber.ciphertext)
                .mod(paillierPublicKey.getNSquared());
        return new EncryptedNumber(paillierPublicKey, encryptedSum);
    }

    public EncryptedNumber mul(BigInteger scalar) {
        BigInteger encryptedMul = ciphertext.modPow(scalar, paillierPublicKey.getNSquared());
        return new EncryptedNumber(paillierPublicKey, encryptedMul);
    }

    public boolean equals(EncryptedNumber other) {
        if (null == other) {
            return false;
        }
        if (!ciphertext.equals(other.getCiphertext())) {
            return false;
        }
        return paillierPublicKey.equals(other.getPublicKey());
    }

}
