package com.silencelaboratories.twopartyecdsa.signature;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;


public class SignMessage2 {

    public final static String PHASE = "sign_message_2";
    private final String sessionId;
    private final ECPoint r2;
    private final DLOGProof dLogProof;

    public SignMessage2(String sessionId, ECPoint r2, DLOGProof dlogProof) {
        this.sessionId = sessionId;
        this.r2 = r2;
        this.dLogProof = dlogProof;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ECPoint getR2() {
        return r2;
    }

    public DLOGProof getdLogProof() {
        return dLogProof;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("r2", ECPointField.toHex(r2));
        m.put("dlog_proof", dLogProof.toJSONObject());
        return m;
    }

    public static SignMessage2 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(SignMessage2.PHASE)) {
                throw new SerializerException("SignMessage1 invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            ECPoint r2 = ECPointField.fromHex(m.getString("r2"));
            DLOGProof dLogProof = DLOGProof.fromJSONObject(m.getJSONObject("dlog_proof"));
            return new SignMessage2(sessionId, r2, dLogProof);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("SignMessage2 fromJSONObject() error", e);
        }
    }

}
