package com.silencelaboratories.twopartyecdsa.phe;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;


public class PaillierPublicKey {

    private final BigInteger g;
    private final BigInteger n;
    private final BigInteger nSquared;
    private final BigInteger maxInt;

    public PaillierPublicKey(BigInteger n) {
        this.g = n.add(BigInteger.ONE);
        this.n = n;
        this.nSquared = n.multiply(n);
        this.maxInt = n.divide(BigInteger.valueOf(3)).subtract(BigInteger.valueOf(1));
    }

    public BigInteger getG() {
        return g;
    }

    public BigInteger getMaxInt() {
        return maxInt;
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getNSquared() {
        return nSquared;
    }

    private static BigInteger randomSampleBelow(BigInteger upperLimit) {
        // return random number in range [1, upperLimit - 1]
        Random randomSource = new SecureRandom();
        BigInteger randomNumber;
        do {
            randomNumber = new BigInteger(upperLimit.bitLength(), randomSource);
        } while ((randomNumber.compareTo(upperLimit) >= 0) ||
                (randomNumber.compareTo(BigInteger.ZERO) == 0));
        return randomNumber;
    }

    public final EncryptedNumber encrypt(BigInteger plaintext) {
        BigInteger r = randomSampleBelow(n);
        return encrypt(plaintext, r);
    }

    public final EncryptedNumber encrypt(BigInteger plaintext, BigInteger r) {
        if (plaintext.compareTo(n) >= 0) {
            throw new RuntimeException(new PaillierException("Plaintext > n"));
        }
        BigInteger result;
        if ((n.subtract(maxInt).compareTo(plaintext) <= 0) && (plaintext.compareTo(n)) < 0) {
            BigInteger negPlaintext = n.subtract(plaintext);
            BigInteger negCiphertext = n.multiply(negPlaintext).add(BigInteger.ONE).mod(nSquared);
            result = negCiphertext.modInverse(nSquared);
        } else {
            // we chose g = n + 1, so that we can exploit the fact that
            // (n+1)^plaintext = n*plaintext + 1 mod n^2
            result = n.multiply(plaintext).add(BigInteger.ONE).mod(nSquared);
        }
        BigInteger obfuscator = r.modPow(n, nSquared);
        result = result.multiply(obfuscator);
        result = result.mod(nSquared);
        return new EncryptedNumber(this, result);
    }

    public boolean equals(PaillierPublicKey other) {
        if (null == other) {
            return false;
        }
        return n.equals(other.getN());
    }

}
