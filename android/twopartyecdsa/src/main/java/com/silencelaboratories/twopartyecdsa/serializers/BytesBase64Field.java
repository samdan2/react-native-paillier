package com.silencelaboratories.twopartyecdsa.serializers;

import org.apache.commons.codec.binary.Base64;

public class BytesBase64Field {

    public static String serialize(byte[] data) {
        return StringField.fromBytes(Base64.encodeBase64(data));
    }

    public static byte[] deserialize(String data) {
        return Base64.decodeBase64(StringField.toBytes(data));
    }

}
