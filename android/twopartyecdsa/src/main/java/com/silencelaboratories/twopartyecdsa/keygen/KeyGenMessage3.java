package com.silencelaboratories.twopartyecdsa.keygen;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;
import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.EncryptedNumberField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPublicKeyField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.NiCorrectKeyProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLwSlackProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLwSlackStatement;
import com.silencelaboratories.twopartyecdsa.zkproofs.wiDLogProof.CompositeDLogProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;


public class KeyGenMessage3 {

    public final static String PHASE = "key_gen_message_3";
    private final String sessionId;
    private final ECPoint q1;
    private final DLOGProof dLogProof1;
    private final BigInteger blindFactor1;
    private final EncryptedNumber cKey;
    private final PaillierPublicKey paillierPublicKey;
    private final NiCorrectKeyProof niCorrectKeyProof;
    private final PDLwSlackStatement pdLwSlackStatement;
    private final PDLwSlackProof pdLwSlackProof;
    private final CompositeDLogProof compositeDLogProof;
    private final ECPoint e1;
    private final DLOGProof dLogProof2;
    private final BigInteger blindFactor2;

    public KeyGenMessage3(
            String sessionId,
            ECPoint q1,
            DLOGProof dLogProof1,
            BigInteger blindFactor1,
            EncryptedNumber cKey,
            PaillierPublicKey paillierPublicKey,
            NiCorrectKeyProof niCorrectKeyProof,
            PDLwSlackStatement pdLwSlackStatement,
            PDLwSlackProof pdLwSlackProof,
            CompositeDLogProof compositeDLogProof,
            ECPoint e1,
            DLOGProof dLogProof2,
            BigInteger blindFactor2) {
        this.sessionId = sessionId;
        this.q1 = q1;
        this.dLogProof1 = dLogProof1;
        this.blindFactor1 = blindFactor1;
        this.cKey = cKey;
        this.paillierPublicKey = paillierPublicKey;
        this.niCorrectKeyProof = niCorrectKeyProof;
        this.pdLwSlackStatement = pdLwSlackStatement;
        this.pdLwSlackProof = pdLwSlackProof;
        this.compositeDLogProof = compositeDLogProof;
        this.e1 = e1;
        this.dLogProof2 = dLogProof2;
        this.blindFactor2 = blindFactor2;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ECPoint getQ1() {
        return q1;
    }

    public DLOGProof getdLogProof1() {
        return dLogProof1;
    }

    public BigInteger getBlindFactor1() {
        return blindFactor1;
    }

    public EncryptedNumber getcKey() {
        return cKey;
    }

    public PaillierPublicKey getPaillierPublicKey() {
        return paillierPublicKey;
    }

    public NiCorrectKeyProof getNiCorrectKeyProof() {
        return niCorrectKeyProof;
    }

    public PDLwSlackStatement getPdLwSlackStatement() {
        return pdLwSlackStatement;
    }

    public PDLwSlackProof getPdLwSlackProof() {
        return pdLwSlackProof;
    }

    public CompositeDLogProof getCompositeDLogProof() {
        return compositeDLogProof;
    }

    public ECPoint getE1() {
        return e1;
    }

    public DLOGProof getdLogProof2() {
        return dLogProof2;
    }

    public BigInteger getBlindFactor2() {
        return blindFactor2;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("q1", ECPointField.serialize(q1));
        m.put("dlog_proof_1", dLogProof1.toJSONObject());
        m.put("blind_factor_1", PosBigIntegerField.toBase64(blindFactor1));
        m.put("c_key", EncryptedNumberField.serialize(cKey));
        m.put("paillier_public_key", PaillierPublicKeyField.serialize(paillierPublicKey));
        m.put("ni_key_correct_proof", niCorrectKeyProof.toJSONObject());
        m.put("pdl_w_slack_statement", pdLwSlackStatement.toJSONObject());
        m.put("pdl_w_slack_proof", pdLwSlackProof.toJSONObject());
        m.put("composite_dlog_proof", compositeDLogProof.toJSONObject());
        m.put("e1", ECPointField.serialize(e1));
        m.put("dlog_proof_2", dLogProof2.toJSONObject());
        m.put("blind_factor_2", PosBigIntegerField.toBase64(blindFactor2));
        return m;
    }

    public static KeyGenMessage3 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(KeyGenMessage3.PHASE)) {
                throw new SerializerException("Invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            PaillierPublicKey paillierPublicKey =
                    PaillierPublicKeyField.deserialize(m.getString("paillier_public_key"));
            ECPoint q1 = ECPointField.deserialize(m.getString("q1"));
            DLOGProof dLogProof1 = DLOGProof.fromJSONObject(m.getJSONObject("dlog_proof_1"));
            BigInteger blindFactor1 = PosBigIntegerField.fromBase64(
                    m.getString("blind_factor_1")
            );
            EncryptedNumber cKey = EncryptedNumberField.deserialize(
                    m.getString("c_key"), paillierPublicKey
            );
            NiCorrectKeyProof niCorrectKeyProof = NiCorrectKeyProof.fromJSONObject(
                    m.getJSONObject("ni_key_correct_proof")
            );
            PDLwSlackStatement pdLwSlackStatement = PDLwSlackStatement.fromJSONObject(
                    m.getJSONObject("pdl_w_slack_statement")
            );
            PDLwSlackProof pdLwSlackProof = PDLwSlackProof.fromJSONObject(
                    m.getJSONObject("pdl_w_slack_proof")
            );
            CompositeDLogProof compositeDLogProof = CompositeDLogProof.fromJSONObject(
                    m.getJSONObject("composite_dlog_proof")
            );
            ECPoint e1 = ECPointField.deserialize(m.getString("e1"));
            DLOGProof dLogProof2 = DLOGProof.fromJSONObject(m.getJSONObject("dlog_proof_2"));
            BigInteger blindFactor2= PosBigIntegerField.fromBase64(
                    m.getString("blind_factor_2")
            );
            return new KeyGenMessage3(
                    sessionId,
                    q1, dLogProof1, blindFactor1,
                    cKey, paillierPublicKey, niCorrectKeyProof,
                    pdLwSlackStatement, pdLwSlackProof, compositeDLogProof,
                    e1, dLogProof2, blindFactor2
            );

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("KeyGenMessage3 fromJSONObject() error: " + e);
        }
    }

}
