package com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof;

import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.EncryptedNumberField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPublicKeyField;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;


public class PDLwSlackStatement {
    private final EncryptedNumber ciphertext;
    private final PaillierPublicKey ek;
    private final ECPoint q;
    private final ECPoint g;
    private final BigInteger h1;
    private final BigInteger h2;
    private final BigInteger nTilde;

    public PDLwSlackStatement(EncryptedNumber ciphertext, PaillierPublicKey ek,
                              ECPoint q, ECPoint g, BigInteger h1, BigInteger h2,
                              BigInteger nTilde) {
        this.ciphertext = ciphertext;
        this.ek = ek;
        this.q = q;
        this.g = g;
        this.h1 = h1;
        this.h2 = h2;
        this.nTilde = nTilde;
    }

    public EncryptedNumber getCiphertext() {
        return ciphertext;
    }

    public PaillierPublicKey getEk() {
        return ek;
    }

    public ECPoint getQ() {
        return q;
    }

    public ECPoint getG() {
        return g;
    }

    public BigInteger getH1() {
        return h1;
    }

    public BigInteger getH2() {
        return h2;
    }

    public BigInteger getNTilde() {
        return nTilde;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("ciphertext", EncryptedNumberField.serialize(ciphertext));
        m.put("ek", PaillierPublicKeyField.serialize(ek));
        m.put("Q", ECPointField.serialize(q));
        m.put("G", ECPointField.serialize(g));
        m.put("h1", PosBigIntegerField.toBase64(h1));
        m.put("h2", PosBigIntegerField.toBase64(h2));
        m.put("N_tilde", PosBigIntegerField.toBase64(nTilde));
        return m;
    }

    public static PDLwSlackStatement fromJSONObject(JSONObject m) throws JSONException {
        PaillierPublicKey ek = PaillierPublicKeyField.deserialize((String) m.get("ek"));
        EncryptedNumber ciphertext = EncryptedNumberField.deserialize(
                (String) m.get("ciphertext"), ek
        );
        ECPoint q = ECPointField.deserialize((String) m.get("Q"));
        ECPoint g = ECPointField.deserialize((String) m.get("G"));
        BigInteger h1 = PosBigIntegerField.fromBase64((String) m.get("h1"));
        BigInteger h2 = PosBigIntegerField.fromBase64((String) m.get("h2"));
        BigInteger nTilde = PosBigIntegerField.fromBase64((String) m.get("N_tilde"));
        return new PDLwSlackStatement(ciphertext, ek, q, g, h1, h2, nTilde);
    }

}
