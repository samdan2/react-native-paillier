package com.silencelaboratories.twopartyecdsa.zkproofs.wiDLogProof;

import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBitLength;

import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.StringField;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class CompositeDLogProof {
//    https://github.com/ZenGo-X/zk-paillier/blob/master/src/zkproofs/wi_dlog_proof.rs
//    Witness Indistinguishable Proof of knowledge of discrete log with composite modulus.
//
//    We follow the Girault’s proof from Pointcheval paper (figure1):
//    https://www.di.ens.fr/david.pointcheval/Documents/Papers/2000_pkcA.pdf
//    The prover wants to prove knowledge of a secret s given a public v = g^-{s} mod N for composite N

    public static class DLogStatement {
        private final BigInteger n;
        private final BigInteger g;
        private final BigInteger ni;

        public DLogStatement(BigInteger n, BigInteger g, BigInteger ni) {
            this.n = n;
            this.g = g;
            this.ni = ni;
        }

        public BigInteger getN() {
            return n;
        }

        public BigInteger getG() {
            return g;
        }

        public BigInteger getNi() {
            return ni;
        }
    }

    private final static int K = 128;
    private final static int K_PRIME = 128;
    private final static int SAMPLE_S = 256;
    private final BigInteger x;
    private final BigInteger y;

    public CompositeDLogProof(BigInteger x, BigInteger y) {
        this.x = x;
        this.y = y;
    }

    private static byte[] computeHash(
            DLogStatement dLogStatement, BigInteger x, String sid, String pid
    ) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            ByteArrayOutputStream digestStream = new ByteArrayOutputStream();
            digestStream.write(PosBigIntegerField.toBytes(x));
            digestStream.write(PosBigIntegerField.toBytes(dLogStatement.g));
            digestStream.write(PosBigIntegerField.toBytes(dLogStatement.n));
            digestStream.write(PosBigIntegerField.toBytes(dLogStatement.ni));
            digestStream.write(StringField.toBytes(sid));
            digestStream.write(StringField.toBytes(pid));
            return digest.digest(digestStream.toByteArray());
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static CompositeDLogProof prove(
            DLogStatement dLogStatement, BigInteger secret, String sid, String pid
    ) {
        BigInteger r = randomSampleBitLength(K + K_PRIME + SAMPLE_S);
        BigInteger x = dLogStatement.g.modPow(r, dLogStatement.n);

        byte[] hash = computeHash(dLogStatement, x, sid, pid);
        BigInteger e = PosBigIntegerField.fromBytes(hash);
        BigInteger y = e.multiply(secret).add(r);
        return new CompositeDLogProof(x, y);
    }

    public boolean verify(DLogStatement dLogStatement, String sid, String pid) {
        if (!(dLogStatement.n.bitLength() > K)) {
            return false;
        }
        if (!dLogStatement.g.gcd(dLogStatement.n).equals(BigInteger.ONE)) {
            return false;
        }
        if (!dLogStatement.ni.gcd(dLogStatement.n).equals(BigInteger.ONE)) {
            return false;
        }
        byte[] hash = computeHash(dLogStatement, x, sid, pid);
        BigInteger e = PosBigIntegerField.fromBytes(hash);
        BigInteger ni_e = dLogStatement.ni.modPow(e, dLogStatement.n);
        BigInteger g_y = dLogStatement.g.modPow(y, dLogStatement.n);
        BigInteger res = g_y.multiply(ni_e).mod(dLogStatement.n);
        return x.equals(res);
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("x", PosBigIntegerField.toBase64(x));
        m.put("y", PosBigIntegerField.toBase64(y));
        return m;
    }

    public static CompositeDLogProof fromJSONObject(JSONObject m) throws JSONException {
        BigInteger x = PosBigIntegerField.fromBase64((String) m.get("x"));
        BigInteger y = PosBigIntegerField.fromBase64((String) m.get("y"));
        return new CompositeDLogProof(x, y);
    }

}
