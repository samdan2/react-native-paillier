package com.silencelaboratories.twopartyecdsa.signature;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;

import org.json.JSONException;
import org.json.JSONObject;


public class SignMessage5 {

    public final static String PHASE = "sign_message_5";
    private final String sessionId;
    private final String signature;

    public SignMessage5(String sessionId, String signature) {
        this.sessionId = sessionId;
        this.signature = signature;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getSignature() {
        return signature;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("signature", signature);
        return m;
    }

    public static SignMessage5 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(SignMessage5.PHASE)) {
                throw new SerializerException("SignMessage5 invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            String signature = m.getString("signature");
            return new SignMessage5(sessionId, signature);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("SignMessage5 fromJSONObject() error", e);
        }
    }

}
