package com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof;

import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBelow;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBitLength;

import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.KeyPair;
import com.silencelaboratories.twopartyecdsa.phe.KeyPairBuilder;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPrivateKey;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;
import com.silencelaboratories.twopartyecdsa.zkproofs.wiDLogProof.CompositeDLogProof;

import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;


public class PDLProof {
//    https://github.com/ZenGo-X/multi-party-ecdsa/blob/master/src/protocols/two_party_ecdsa/lindell_2017/party_one.rs
//    line 366 pdl_proof()
//    https://github.com/ZenGo-X/multi-party-ecdsa/blob/master/src/protocols/two_party_ecdsa/lindell_2017/party_two.rs
//    line 275 pdl_verify()

    private final PDLwSlackStatement pdLwSlackStatement;
    private final PDLwSlackProof pdLwSlackProof;
    private final CompositeDLogProof compositeDLogProof;

    public PDLProof(PDLwSlackStatement pdLwSlackStatement,
                    PDLwSlackProof pdLwSlackProof,
                    CompositeDLogProof compositeDLogProof) {
        this.pdLwSlackStatement = pdLwSlackStatement;
        this.pdLwSlackProof = pdLwSlackProof;
        this.compositeDLogProof = compositeDLogProof;
    }

    public PDLwSlackStatement getPdLwSlackStatement() {
        return pdLwSlackStatement;
    }

    public PDLwSlackProof getPdLwSlackProof() {
        return pdLwSlackProof;
    }

    public CompositeDLogProof getCompositeDLogProof() {
        return compositeDLogProof;
    }

    public static PDLProof prove(
            BigInteger x_1, BigInteger cKeyRandomness,
            PaillierPublicKey ek, EncryptedNumber encryptedShare,
            String sid, String pid) {

        // n_tilde, h1, h2, xhi = generate_h1_h2_n_tilde()
        KeyPair keyPair = KeyPairBuilder.generateKeyPair();
        PaillierPublicKey ekTilde = keyPair.getPublicKey();
        PaillierPrivateKey dkTilde = keyPair.getPrivateKey();
        BigInteger phi = dkTilde.getP().subtract(BigInteger.ONE).
                multiply(dkTilde.getQ().subtract(BigInteger.ONE));
        BigInteger h1 = randomSampleBelow(phi);
        BigInteger xhi = randomSampleBitLength(256);
        BigInteger h1Inv = h1.modInverse(ekTilde.getN());
        BigInteger h2 = h1Inv.modPow(xhi, ekTilde.getN());
        BigInteger nTilde = ekTilde.getN();

        CompositeDLogProof.DLogStatement dLogStatement = new CompositeDLogProof.DLogStatement(
                nTilde, h1, h2
        );
        CompositeDLogProof compositeDLogProof = CompositeDLogProof.prove(
                dLogStatement, xhi, sid, pid
        );

        // Generate PDL with slack statement, witness and proof
        ECPoint curveG = CurveSecp256k1.basePointG();
        PDLwSlackStatement pdLwSlackStatement = new PDLwSlackStatement(
                encryptedShare,
                ek,
                curveG.multiply(x_1),
                curveG,
                dLogStatement.getG(),
                dLogStatement.getNi(),
                dLogStatement.getN()
        );
        PDLwSlackProof.PDLwSlackWitness pdLwSlackWitness = new PDLwSlackProof.PDLwSlackWitness(
                x_1,
                cKeyRandomness
        );
        PDLwSlackProof pdLwSlackProof = PDLwSlackProof.prove(
                pdLwSlackWitness, pdLwSlackStatement, sid, pid
        );
        return new PDLProof(pdLwSlackStatement, pdLwSlackProof, compositeDLogProof);
    }

    public boolean verify(
            PaillierPublicKey paillierPublicKey,
            EncryptedNumber encryptedSecretShare, ECPoint q1,
            String sid, String pid) {
        if (!pdLwSlackStatement.getEk().equals(paillierPublicKey)) {
            return false;
        }
        if (!pdLwSlackStatement.getCiphertext().equals(encryptedSecretShare)) {
            return false;
        }
        if (!pdLwSlackStatement.getQ().equals(q1)) {
            return false;
        }
        CompositeDLogProof.DLogStatement dLogStatement = new CompositeDLogProof.DLogStatement(
                pdLwSlackStatement.getNTilde(),
                pdLwSlackStatement.getH1(),
                pdLwSlackStatement.getH2()
        );
        boolean cond1 = compositeDLogProof.verify(dLogStatement, sid, pid);
        boolean cond2 = pdLwSlackProof.verify(pdLwSlackStatement, sid, pid);
        return (cond1 && cond2);
    }

}
