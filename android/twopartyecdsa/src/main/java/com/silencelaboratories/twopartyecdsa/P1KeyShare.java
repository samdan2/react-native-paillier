package com.silencelaboratories.twopartyecdsa;

import com.silencelaboratories.twopartyecdsa.phe.PaillierPrivateKey;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPrivateKeyField;
import com.silencelaboratories.twopartyecdsa.serializers.PaillierPublicKeyField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;

/**
 * Party1 KeyShare class as result of TSS(2,2) DKG
 */
public class P1KeyShare {

    private final BigInteger x1;
    private final ECPoint publicKey;
    private final PaillierPrivateKey paillierPrivateKey;
    private final PaillierPublicKey paillierPublicKey;

    public P1KeyShare(
            BigInteger x1,
            ECPoint publicKey,
            PaillierPrivateKey paillierPrivateKey,
            PaillierPublicKey paillierPublicKey) {
        this.x1 = x1;
        this.publicKey = publicKey;
        this.paillierPrivateKey = paillierPrivateKey;
        this.paillierPublicKey = paillierPublicKey;
    }

    public BigInteger getX1() {
        return x1;
    }

    public ECPoint getPublicKey() {
        return publicKey;
    }

    public String getPublicKeyHex() {
        return ECPointField.toHex(publicKey);
    }

    public PaillierPrivateKey getPaillierPrivateKey() {
        return paillierPrivateKey;
    }

    public PaillierPublicKey getPaillierPublicKey() {
        return paillierPublicKey;
    }

    /**
     * Serializer method for P1KeyShare
     *
     * @return JSONObject
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("x1", PosBigIntegerField.toHex(x1));
        m.put("public_key", ECPointField.toHex(publicKey));
        m.put("paillier_private_key", PaillierPrivateKeyField.serialize(paillierPrivateKey));
        m.put("paillier_public_key", PaillierPublicKeyField.serialize(paillierPublicKey));
        return m;
    }

    /**
     * Deserializer method for P1KeyShare
     *
     * @param m JSONObject from toJSONObject() method
     * @return P1KeyShare object
     * @throws JSONException
     */
    public static P1KeyShare fromJSONObject(JSONObject m) throws JSONException {
        BigInteger x1 = PosBigIntegerField.fromHex(m.getString("x1"));
        ECPoint publicKey = ECPointField.fromHex(m.getString("public_key"));
        PaillierPrivateKey paillierPrivateKey = PaillierPrivateKeyField.deserialize(
                m.getJSONObject("paillier_private_key")
        );
        PaillierPublicKey paillierPublicKey = PaillierPublicKeyField.deserialize(
                m.getString("paillier_public_key")
        );
        return new P1KeyShare(x1, publicKey, paillierPrivateKey, paillierPublicKey);
    }

}
