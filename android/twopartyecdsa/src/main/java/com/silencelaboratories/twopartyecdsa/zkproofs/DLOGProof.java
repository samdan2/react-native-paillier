package com.silencelaboratories.twopartyecdsa.zkproofs;

import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomCurveScalar;

import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.StringField;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class DLOGProof {

    private final ECPoint t;
    private final BigInteger s;
    private final ECPoint curveG = CurveSecp256k1.basePointG();
    private final BigInteger curveQ = CurveSecp256k1.order();

    public DLOGProof(ECPoint t, BigInteger s) {
        this.t = t;
        this.s = s;
    }

    public ECPoint getT() {
        return t;
    }

    public BigInteger getS() {
        return s;
    }

    private static BigInteger hashPoints(
            ECPoint point1, ECPoint point2, ECPoint point3, String sid, String pid
    ) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(PosBigIntegerField.toBytes(
                    point1.normalize().getXCoord().toBigInteger())
            );
            outputStream.write(PosBigIntegerField.toBytes(
                    point2.normalize().getXCoord().toBigInteger())
            );
            outputStream.write(PosBigIntegerField.toBytes(
                    point3.normalize().getXCoord().toBigInteger())
            );
            outputStream.write(StringField.toBytes(sid));
            outputStream.write(StringField.toBytes(pid));
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(outputStream.toByteArray());
            return PosBigIntegerField.fromBytes(hash);
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static DLOGProof prove(BigInteger x, ECPoint y, String sid, String pid) {
        // y = x*G
        ECPoint curveG = CurveSecp256k1.basePointG();
        BigInteger curveQ = CurveSecp256k1.order();
        BigInteger r = randomCurveScalar();
        ECPoint t = curveG.multiply(r).normalize();
        BigInteger c = hashPoints(curveG, y, t, sid, pid);
        BigInteger s = c.multiply(x).add(r).mod(curveQ);
        return new DLOGProof(t, s);
    }

    public boolean verify(ECPoint y, String sid, String pid) {
        BigInteger c = hashPoints(curveG, y, t, sid, pid);
        ECPoint lhs = curveG.multiply(s).normalize();
        ECPoint rhs = y.multiply(c).add(t).normalize();
        return lhs.equals(rhs);
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("t", ECPointField.serialize(t));
        m.put("s", PosBigIntegerField.toBase64(s));
        return m;
    }

    public static DLOGProof fromJSONObject(JSONObject m) throws JSONException {
        ECPoint t = ECPointField.deserialize((String) m.get("t"));
        BigInteger s = PosBigIntegerField.fromBase64((String) m.get("s"));
        return new DLOGProof(t, s);
    }

}
