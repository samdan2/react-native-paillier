package com.silencelaboratories.twopartyecdsa.phe;


import java.math.BigInteger;
import java.util.Objects;


public class PaillierPrivateKey {

    private final PaillierPublicKey paillierPublicKey;
    private final BigInteger p;
    private final BigInteger q;
    private final BigInteger pSquare;
    private final BigInteger qSquare;
    private final BigInteger pInverse;
    private final BigInteger hp;
    private final BigInteger hq;

    public PaillierPrivateKey(PaillierPublicKey paillierPublicKey, BigInteger p, BigInteger q) {
        this.paillierPublicKey = paillierPublicKey;
        // ensure that p < q.
        if (q.compareTo(p) < 0) {
            this.p = q;
            this.q = p;
        } else {
            this.p = p;
            this.q = q;
        }
        pSquare = this.p.multiply(this.p);
        qSquare = this.q.multiply(this.q);
        pInverse = this.p.modInverse(this.q);
        hp = hFunction(this.p, pSquare);
        hq = hFunction(this.q, qSquare);
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public PaillierPublicKey getPublicKey() {
        return paillierPublicKey;
    }

    public BigInteger decrypt(EncryptedNumber encryptedNumber) throws PaillierException {
        if (!Objects.equals(paillierPublicKey.getN(), encryptedNumber.getPublicKey().getN())) {
            throw new PaillierException("Public keys not match");
        }
        BigInteger encoding = rawDecrypt(encryptedNumber.getCiphertext());
        if (encoding.compareTo(paillierPublicKey.getN()) >= 0) {
            throw new PaillierException("Decrypted number > n");
        }
        if (encoding.compareTo(paillierPublicKey.getMaxInt()) <= 0) {
            // Positive
            return encoding;
        }
        // Negative
        return encoding.subtract(paillierPublicKey.getN());
    }

    private BigInteger rawDecrypt(BigInteger ciphertext) {
        BigInteger decryptToP = lFunction(ciphertext.modPow(p.subtract(BigInteger.ONE), pSquare), p).multiply(hp).mod(p);
        BigInteger decryptToQ = lFunction(ciphertext.modPow(q.subtract(BigInteger.ONE), qSquare), q).multiply(hq).mod(q);
        return crt(decryptToP, decryptToQ);
    }

    private BigInteger hFunction(BigInteger x, BigInteger xSquare) {
        // Computes the h-function as defined in Paillier's paper page 12,
        // 'Decryption using Chinese-remaindering'
        return lFunction(paillierPublicKey.getG().modPow(x.subtract(BigInteger.ONE), xSquare), x).modInverse(x);
    }

    private BigInteger lFunction(BigInteger x, BigInteger p) {
        // Computes the L function as defined in Paillier's paper. That is: L(x,p) = (x-1)/p
        BigInteger result = x.subtract(BigInteger.ONE);
        return result.divide(p);
    }

    private BigInteger crt(BigInteger mp, BigInteger mq) {
        // The Chinese Remainder Theorem as needed for decryption. Returns the solution modulo n=pq.
        // Args:
        //   mp(int): the solution modulo p.
        //   mq(int): the solution modulo q.
        BigInteger diff = mq.subtract(mp);
        BigInteger u = diff.multiply(pInverse).mod(q);
        return u.multiply(p).add(mp);
    }

}
