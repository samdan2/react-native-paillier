package com.silencelaboratories.twopartyecdsa.keygen;


import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_1;
import static com.silencelaboratories.twopartyecdsa.Common.PARTY_ID_2;
import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomCurveScalar;
import static com.silencelaboratories.twopartyecdsa.zkproofs.HashCommitment.verifyCommitment;

import com.silencelaboratories.twopartyecdsa.P2KeyShare;
import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;
import com.silencelaboratories.twopartyecdsa.utils.Pair;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.NiCorrectKeyProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLwSlackProof;
import com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof.PDLwSlackStatement;
import com.silencelaboratories.twopartyecdsa.zkproofs.wiDLogProof.CompositeDLogProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Objects;

/**
 * Party2 DKG class for TSS(2,2)
 */
public class P2KeyGen {

    enum Party2KeyGenState {
        COMPLETE,
        FAILED,
        PROCESS_KEY_GEN_MSG_1,
        PROCESS_KEY_GEN_MSG_3
    }

    private final ECPoint curveG = CurveSecp256k1.basePointG();
    private final BigInteger curveQ = CurveSecp256k1.order();

    private final String sessionId;
    private BigInteger x2;
    private BigInteger eph2;
    private String commitment1;
    private String commitment2;
    private String expectedPublicKey;
    private Party2KeyGenState state;

    /**
     * P2KeyGen constructor
     *
     * @param  sessionId Session ID for DKG action
     * @param  x2 Secret bytes of Party2, private_key = (x1 + x2) mod q
     * @param  expectedPublicKey hex string of public_key, for key recovery and refresh purposes
     * @throws KeyGenFailed
     */
    public P2KeyGen(String sessionId, byte[] x2, String expectedPublicKey) throws KeyGenFailed {
        this.sessionId = sessionId;
        this.expectedPublicKey = expectedPublicKey;
        if (x2 != null) {
            if (x2.length != 32) {
                throw new KeyGenFailed("Invalid length of x2");
            }
            this.x2 = PosBigIntegerField.fromBytes(x2).mod(curveQ);
        }
        else {
            this.x2 = randomCurveScalar();
        }
        state = Party2KeyGenState.PROCESS_KEY_GEN_MSG_1;
    }

    /**
     * P2KeyGen constructor
     *
     * @param  sessionId Session ID for DKG action
     * @param  x2 Secret bytes of Party2, private_key = (x1 + x2) mod q
     * @throws KeyGenFailed
     */
    public P2KeyGen(String sessionId, byte[] x2) throws KeyGenFailed {
        this(sessionId, x2, null);
    }

    /**
     * P2KeyGen constructor
     * <p>
     * Initializes P2KeyGen instance with random x2
     *
     * @param  sessionId Session ID for DKG action
     * @throws KeyGenFailed
     */
    public P2KeyGen(String sessionId) throws KeyGenFailed {
        this(sessionId, null, null);
    }

    private P2KeyGen(
            String sessionId,
            BigInteger x2,
            BigInteger eph2,
            String commitment1,
            String commitment2,
            String expectedPublicKey,
            Party2KeyGenState state) {
        this.sessionId = sessionId;
        this.x2 = x2;
        this.eph2 = eph2;
        this.commitment1 = commitment1;
        this.commitment2 = commitment2;
        this.expectedPublicKey = expectedPublicKey;
        this.state = state;
    }

    /**
     * Serializer method for P2KeyGen
     *
     * @return JSONObject
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("session_id", sessionId);
        m.put("x2", PosBigIntegerField.toBase64(x2));
        m.put("eph2", JSONObject.NULL);
        m.put("commitment1", JSONObject.NULL);
        m.put("commitment2", JSONObject.NULL);
        m.put("expectedPublicKey", JSONObject.NULL);
        m.put("state", state);

        if (eph2 != null)
            m.put("eph2", PosBigIntegerField.toBase64(eph2));
        if (commitment1 != null)
            m.put("commitment1", commitment1);
        if (commitment2 != null)
            m.put("commitment2", commitment2);
        if (expectedPublicKey != null)
            m.put("expectedPublicKey", expectedPublicKey);

        return m;
    }

    /**
     * Deserializer method for P2KeyGen
     *
     * @param m JSONObject from toJSONObject() method
     * @return P2KeyGen object
     * @throws JSONException
     */
    public static P2KeyGen fromJSONObject(JSONObject m) throws JSONException {
        String sessionId = m.getString("session_id");
        BigInteger x2 = PosBigIntegerField.fromBase64(m.getString("x2"));
        String stateStr = m.getString("state");
        Party2KeyGenState state;
        switch (stateStr) {
            case ("COMPLETE"):
                state = Party2KeyGenState.COMPLETE;
                break;
            case ("PROCESS_KEY_GEN_MSG_1"):
                state = Party2KeyGenState.PROCESS_KEY_GEN_MSG_1;
                break;
            case ("PROCESS_KEY_GEN_MSG_3"):
                state = Party2KeyGenState.PROCESS_KEY_GEN_MSG_3;
                break;
            default:
                state = Party2KeyGenState.FAILED;
                break;
        }

        BigInteger eph2 = null;
        String commitment1 = null;
        String commitment2 = null;
        String expectedPublicKey = null;

        if (!m.isNull("eph2"))
            eph2 = PosBigIntegerField.fromBase64(m.getString("eph2"));
        if (!m.isNull("commitment1"))
            commitment1 = m.getString("commitment1");
        if (!m.isNull("commitment2"))
            commitment2 = m.getString("commitment2");
        if (!m.isNull("expectedPublicKey"))
            expectedPublicKey = m.getString("expectedPublicKey");

        return new P2KeyGen(
                sessionId, x2, eph2, commitment1, commitment2, expectedPublicKey, state
        );
    }

    /**
     * Initializes P2KeyGen for Key Refresh purpose
     *
     * @param  sessionId Session ID for DKG action
     * @param  p2KeyShare Existed P2KeyShare object to refresh
     * @return P2KeyGen object
     * @throws KeyGenFailed
     * @throws JSONException
     */
    public static P2KeyGen getInstanceForKeyRefresh(String sessionId, P2KeyShare p2KeyShare)
            throws KeyGenFailed, JSONException {
        byte[] x2 = PosBigIntegerField.toBytes(p2KeyShare.getX2());
        String expectedPublicKey = ECPointField.toHex(p2KeyShare.getPublicKey());
        return new P2KeyGen(sessionId, x2, expectedPublicKey);
    }

    public boolean isActive() {
        boolean cond1 = (state != Party2KeyGenState.FAILED);
        boolean cond2 = (state != Party2KeyGenState.COMPLETE);
        return cond1 && cond2;
    }

    private KeyGenMessage2 processMessage1(final KeyGenMessage1 keyGenMsg1)
            throws KeyGenFailed {
        if (state != Party2KeyGenState.PROCESS_KEY_GEN_MSG_1) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid state");
        }
        if (!sessionId.equals(keyGenMsg1.getSessionId())) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid sessionId");
        }

        this.commitment1 = keyGenMsg1.getCommitment1();
        this.commitment2 = keyGenMsg1.getCommitment2();

        ECPoint q2 = curveG.multiply(x2);
        DLOGProof dLogProof1 = DLOGProof.prove(x2, q2, sessionId, PARTY_ID_2);

        eph2 = randomCurveScalar();
        ECPoint e2 = curveG.multiply(eph2);
        DLOGProof dLogProof2 = DLOGProof.prove(eph2, e2, sessionId, PARTY_ID_2);

        KeyGenMessage2 keyGenMsg2 = new KeyGenMessage2(sessionId, q2, dLogProof1, e2, dLogProof2);
        state = Party2KeyGenState.PROCESS_KEY_GEN_MSG_3;
        return keyGenMsg2;
    }

    private P2KeyShare processMessage3(final KeyGenMessage3 keyGenMsg3)
            throws KeyGenFailed {
        if (state != Party2KeyGenState.PROCESS_KEY_GEN_MSG_3) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid state");
        }
        if (!sessionId.equals(keyGenMsg3.getSessionId())) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid sessionId");
        }

        ECPoint q1 = keyGenMsg3.getQ1();
        DLOGProof dLogProof1 = keyGenMsg3.getdLogProof1();
        boolean cond1 = dLogProof1.verify(q1, sessionId, PARTY_ID_1);
        if (!cond1) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid dLogProof1");
        }
        BigInteger blindFactor1 = keyGenMsg3.getBlindFactor1();
        boolean cond2 = verifyCommitment(
                this.commitment1, q1, dLogProof1, blindFactor1, sessionId, PARTY_ID_1
        );
        if (!cond2) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid Commitment1");
        }

        ECPoint publicKey = q1.multiply(x2);
        if (expectedPublicKey != null) {
            if (!expectedPublicKey.equals(ECPointField.toHex(publicKey))) {
                state = Party2KeyGenState.FAILED;
                throw new KeyGenFailed("Invalid publicKey");
            }
        }

        ECPoint e1 = keyGenMsg3.getE1();
        DLOGProof dLogProof2 = keyGenMsg3.getdLogProof2();
        boolean cond3 = dLogProof2.verify(e1, sessionId, PARTY_ID_1);
        if (!cond3) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid dLogProof2");
        }
        BigInteger blindFactor2 = keyGenMsg3.getBlindFactor2();
        boolean cond4 = verifyCommitment(
                this.commitment2, e1, dLogProof2, blindFactor2, sessionId, PARTY_ID_1
        );
        if (!cond4) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid Commitment2");
        }
        // rotate private share x2 using rotateValue2
        ECPoint ephPoint = e1.multiply(eph2);
        BigInteger rotateValue1 = ephPoint.normalize().getXCoord().toBigInteger();
        BigInteger rotateValue2 = rotateValue1.modInverse(curveQ);
        // rotate q1 using rotateValue1 to calculate PDLProof correctly, since x1 has been rotated
        ECPoint q1Rotated = q1.multiply(rotateValue1);
        x2 = x2.multiply(rotateValue2).mod(curveQ);

        PaillierPublicKey paillierPublicKey = keyGenMsg3.getPaillierPublicKey();
        boolean cond5 = (paillierPublicKey.getN().bitLength() == 2048);
        if (!cond5) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("PaillierPublicKey.bitLength !== 2048");
        }
        NiCorrectKeyProof nIKeyCorrectProof = keyGenMsg3.getNiCorrectKeyProof();
        boolean cond6 = nIKeyCorrectProof.verify(paillierPublicKey, sessionId, PARTY_ID_1);
        if (!cond6) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid nIKeyCorrectProof");
        }
        EncryptedNumber cKeyX1 = keyGenMsg3.getcKey();
        PDLwSlackStatement pDSwSlackStatement = keyGenMsg3.getPdLwSlackStatement();
        PDLwSlackProof pDLwSlackProof = keyGenMsg3.getPdLwSlackProof();
        CompositeDLogProof compositeDLogProof = keyGenMsg3.getCompositeDLogProof();
        PDLProof pDLProof = new PDLProof(pDSwSlackStatement, pDLwSlackProof, compositeDLogProof);
        boolean cond7 = pDLProof.verify(
                paillierPublicKey,
                cKeyX1,
                q1Rotated,
                this.sessionId,
                PARTY_ID_1
        );
        if (!cond7) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed("Invalid PDLProof");
        }
        P2KeyShare p2KeyShare = new P2KeyShare(this.x2, publicKey, cKeyX1, paillierPublicKey);
        state = Party2KeyGenState.COMPLETE;
        return p2KeyShare;
    }

    /**
     * Processes messages of DKG protocol
     *
     * @param  message JSONObject
     * @return pair of the message object to send and
     * the P2KeyShare object after the DKG is complete
     * @throws KeyGenFailed
     */
    public Pair<JSONObject, P2KeyShare> processMessage(JSONObject message)
            throws KeyGenFailed {
        if (!this.isActive()) {
            throw new KeyGenFailed("KeyGen was already Completed or Failed");
        }

        String sessionId = message.getString("session_id");
        if (!Objects.equals(this.sessionId, sessionId))
            throw new KeyGenFailed("Invalid sessionId");

        try {
            if (state == Party2KeyGenState.PROCESS_KEY_GEN_MSG_1) {
                KeyGenMessage1 keyGenMsg1 = KeyGenMessage1.fromJSONObject(message);
                KeyGenMessage2 keyGenMsg2 = processMessage1(keyGenMsg1);
                return new Pair<>(keyGenMsg2.toJSONObject(), null);
            }
            if (state == Party2KeyGenState.PROCESS_KEY_GEN_MSG_3) {
                KeyGenMessage3 keyGenMsg3 = KeyGenMessage3.fromJSONObject(message);
                P2KeyShare p2KeyShare = processMessage3(keyGenMsg3);
                return new Pair<>(null, p2KeyShare);
            }
        } catch (Exception e) {
            state = Party2KeyGenState.FAILED;
            throw new KeyGenFailed(e);
        }
        state = Party2KeyGenState.FAILED;
        throw new KeyGenFailed("");
    }

}
