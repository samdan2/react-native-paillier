package com.silencelaboratories.twopartyecdsa.signature;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;
import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.EncryptedNumberField;

import org.json.JSONException;
import org.json.JSONObject;


public class SignMessage4 {

    public final static String PHASE = "sign_message_4";
    private final String sessionId;
    private final EncryptedNumber c3;

    public SignMessage4(String sessionId, EncryptedNumber c3) {
        this.sessionId = sessionId;
        this.c3 = c3;
    }

    public String getSessionId() {
        return sessionId;
    }

    public EncryptedNumber getC3() {
        return c3;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("c3", EncryptedNumberField.serialize(c3));
        return m;
    }

    public static SignMessage4 fromJSONObject(JSONObject m, PaillierPublicKey paillierPublicKey)
            throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(SignMessage4.PHASE)) {
                throw new SerializerException("SignMessage3 invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            EncryptedNumber c3 = EncryptedNumberField.deserialize(
                    m.getString("c3"), paillierPublicKey
            );
            return new SignMessage4(sessionId, c3);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("SignMessage3 fromJSONObject() error", e);
        }
    }

}
