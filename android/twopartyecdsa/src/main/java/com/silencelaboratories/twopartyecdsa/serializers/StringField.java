package com.silencelaboratories.twopartyecdsa.serializers;

import java.nio.charset.StandardCharsets;

public class StringField {

    public static byte[] toBytes(String data) {
        return data.getBytes(StandardCharsets.UTF_8);
    }

    public static String fromBytes(byte[] data) {
        return new String(data, StandardCharsets.UTF_8);
    }

    public static String serialize(String data) {
        return data;
    }

    public static String deserialize(String data) {
        return data;
    }
}
