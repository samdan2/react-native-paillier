package com.silencelaboratories.twopartyecdsa.signature;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;

import org.json.JSONException;
import org.json.JSONObject;


public class SignMessage1 {

    public final static String PHASE = "sign_message_1";
    private final String sessionId;
    private final String commitment;

    public SignMessage1(String sessionId, String commitment) {
        this.sessionId = sessionId;
        this.commitment = commitment;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getCommitment() {
        return commitment;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("commitment", commitment);
        return m;
    }

    public static SignMessage1 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(SignMessage1.PHASE)) {
                throw new SerializerException("SignMessage1 invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            String commitment = m.getString("commitment");
            return new SignMessage1(sessionId, commitment);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("SignMessage1 fromJSONObject() error", e);
        }
    }

}
