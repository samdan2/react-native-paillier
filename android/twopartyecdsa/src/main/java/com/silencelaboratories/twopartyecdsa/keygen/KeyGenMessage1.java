package com.silencelaboratories.twopartyecdsa.keygen;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;

import org.json.JSONException;
import org.json.JSONObject;


public class KeyGenMessage1 {

    public final static String PHASE = "key_gen_message_1";
    private final String sessionId;
    private final String commitment1;
    private final String commitment2;

    public KeyGenMessage1(String sessionId, String commitment1, String commitment2) {
        this.sessionId = sessionId;
        this.commitment1 = commitment1;
        this.commitment2 = commitment2;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getCommitment1() {
        return commitment1;
    }

    public String getCommitment2() {
        return commitment2;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("commitment_1", commitment1);
        m.put("commitment_2", commitment2);
        return m;
    }

    public static KeyGenMessage1 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(KeyGenMessage1.PHASE)) {
                throw new SerializerException("Invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            String commitment1 = m.getString("commitment_1");
            String commitment2 = m.getString("commitment_2");
            return new KeyGenMessage1(sessionId, commitment1, commitment2);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("KeyGenMessage1 fromJSONObject() error: " + e);
        }
    }

}
