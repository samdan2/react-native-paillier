package com.silencelaboratories.twopartyecdsa.phe;


public class PaillierException extends Exception {
    public PaillierException(String message) {
        super(message);
    }
}
