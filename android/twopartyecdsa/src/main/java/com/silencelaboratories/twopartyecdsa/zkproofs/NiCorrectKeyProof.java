package com.silencelaboratories.twopartyecdsa.zkproofs;

import com.silencelaboratories.twopartyecdsa.phe.PaillierPrivateKey;
import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.StringField;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class NiCorrectKeyProof {
//    Non-interactive Correct Paillier Public Key Proof
//    https://github.com/ZenGo-X/zk-paillier/blob/master/src/zkproofs/correct_key_ni.rs
//
//    This protocol is based on the NIZK protocol in https://eprint.iacr.org/2018/057.pdf
//    for parameters = e = N, m2 = 11, alpha = 6370 see https://eprint.iacr.org/2018/987.pdf 6.2.3 for full details
//
//    More links:
//    https://github.com/mortendahl/rust-paillier/blob/master/src/proof/correct_key.rs

    private final static String SALT = "SilenceLaboratories";
    private final static int M2 = 11;
    private final static int DIGEST_SIZE = 256;
    private final static BigInteger ALPHA_PRIMORIAL = new BigInteger("44871651744009136248115543081640547413785854417842050160655833875792914833852769205831424979368719986889519256934239452438251108738670217298542180982547421007901019408155961940142468907900676141149633188172029947498666222471142795699128314649438784106402197023949268047384343715946006767671319388463922366703585708460135453240679421061304864609915827908896062350138633849514905858373339528086006145373712431756746905467935232935398951226852071323775412278763371089401544920873813490290672436809231516731065356763193493525160238868779310055137922174496115680527519932793977258424479253973670103634070028863591207614649216492780891961054287421831028229266989697058385612003557825398202548657910983931484180193293615175594925895929359108723671212631368891689462486968022029482413912928883488902454913524492340322599922718890878760895105937402913873414377276608236656947832307175090505396675623505955607363683869194683635689701238311577953994900734498406703176954324494694474545570839360607926610248093452739817614097197031607820417729009847465138388398887861935127785385309564525648905444610640901769290645369888935446477559073843982605496992468605588284307311971153579731703863970674466666844817336319390617551354845025116350295041840093627836067370100384861820888752358520276041000456608056339377573485917445104757987800101659688183150320442308091835974182809184299472568260682774683272697993855730500061223160274918361373258473553412704497335663924406111413972911417644029226449602417135116011968946232623154008710271296183350215563946003547561056456285939676838623311370087238225630994506113422922846572616538637723054222166159389475617214681282874373185283568512603887750846072033376432252677883915884203823739988948315257311383912016966925295975180180438969999175030785077627458887411146486902613291202008193902979800279637509789564807502239686755727063367075758492823731724669702442450502667810890608807091448688985203084972035197770874223259420649055450382725355162738490355628688943706634905982449810389530661328557381850782677221561924983234877936783136471890539395124220965982831778882400224156689487137227198030461624542872774217771594215907203725682315714199249588874271661233929713660269883273404764648327455796699366900022345171030564747210542398285078804310752063852249740561571105640741618793118627170070315410588646442647771802031066589341358879304845579387079972404386434238273904239604603511925708377008467129590636257287965232576327580009018475271364237665836186806027331208426256451429549641988386585949300254487647395222785274120561299318070944530096970076560461229486504018773252771360855091191876004370694539453020462096690084476681253865429278552786361828508910022714749051734108364178374765700925133405508684883070");
    private List<BigInteger> sigmaVec;

    public NiCorrectKeyProof(List<BigInteger> sigmaVec) {
        this.sigmaVec = sigmaVec;
    }

    private static BigInteger maskGeneration(int outLength, byte[] seed) {
        try {
            int msklen = outLength / DIGEST_SIZE + 1;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            MessageDigest digest;
            ByteArrayOutputStream digestStream;
            for (int i = 0; i < msklen; i++) {
                digest = MessageDigest.getInstance("SHA-256");
                digestStream = new ByteArrayOutputStream();
                digestStream.write(seed);
                digestStream.write(PosBigIntegerField.toBytes(BigInteger.valueOf(i), 4));
                byte[] hash = digest.digest(digestStream.toByteArray());
                outputStream.write(hash);
            }
            return PosBigIntegerField.fromBytes(outputStream.toByteArray());
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<BigInteger> rhoVec(
            BigInteger publicN, int keyLength, String sid, String pid
    ) {
        try {
            List<BigInteger> resultVector = new ArrayList<>();
            MessageDigest digest;
            ByteArrayOutputStream digestStream;
            byte[] sidBytes = StringField.toBytes(sid);
            byte[] pidBytes = StringField.toBytes(pid);
            for (int i = 0; i < M2; i++) {
                digest = MessageDigest.getInstance("SHA-256");
                digestStream = new ByteArrayOutputStream();
                digestStream.write(PosBigIntegerField.toBytes(publicN));
                digestStream.write(SALT.getBytes(StandardCharsets.UTF_8));
                digestStream.write(PosBigIntegerField.toBytes(BigInteger.valueOf(i), 4));
                digestStream.write(sidBytes);
                digestStream.write(pidBytes);
                byte[] seedBn = digest.digest(digestStream.toByteArray());
                BigInteger value = maskGeneration(keyLength, seedBn).mod(publicN);
                resultVector.add(value);
            }
            return resultVector;
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static BigInteger crtRecombine(BigInteger rp, BigInteger rq, BigInteger p,
                                                 BigInteger q, BigInteger pInv) {
        BigInteger diff = rq.subtract(rp).mod(q);
        BigInteger u = diff.multiply(pInv).mod(q);
        return u.multiply(p).add(rp);
    }

    private static BigInteger extractNRoot(
            PaillierPrivateKey paillierPrivateKey, BigInteger value
    ) {
        BigInteger p = paillierPrivateKey.getP();
        BigInteger q = paillierPrivateKey.getQ();
        BigInteger zp = value.mod(p);
        BigInteger zq = value.mod(q);

        BigInteger n = p.multiply(q);
        BigInteger pMinusOne = p.subtract(BigInteger.ONE);
        BigInteger qMinusOne = q.subtract(BigInteger.ONE);
        BigInteger phi = pMinusOne.multiply(qMinusOne);
        BigInteger dn = n.modInverse(phi);
        BigInteger dp = dn.mod(pMinusOne);
        BigInteger dq = dn.mod(qMinusOne);
        BigInteger pInv = p.modInverse(q);

        BigInteger rp = zp.modPow(dp, p);
        BigInteger rq = zq.modPow(dq, q);
        return crtRecombine(rp, rq, p, q, pInv);
    }

    public static NiCorrectKeyProof prove(
            PaillierPrivateKey paillierPrivateKey, String sid, String pid
    ) {
        PaillierPublicKey paillierPublicKey = paillierPrivateKey.getPublicKey();
        BigInteger publicN = paillierPublicKey.getN();
        int keyLength = publicN.bitLength();
        List<BigInteger> rhoVector = rhoVec(publicN, keyLength, sid, pid);
        List<BigInteger> sigmaVec = new ArrayList<>();
        for (BigInteger rhoValue : rhoVector) {
            sigmaVec.add(extractNRoot(paillierPrivateKey, rhoValue));
        }
        return new NiCorrectKeyProof(sigmaVec);
    }

    public boolean verify(
            PaillierPublicKey paillierPublicKey, String sid, String pid
    ) {
        BigInteger publicN = paillierPublicKey.getN();
        int keyLength = publicN.bitLength();
        List<BigInteger> rhoVector = rhoVec(publicN, keyLength, sid, pid);
        BigInteger gcdTest = ALPHA_PRIMORIAL.gcd(publicN);
        List<BigInteger> derivedRhoVector = new ArrayList<>();
        for (BigInteger item : sigmaVec) {
            derivedRhoVector.add(item.modPow(publicN, publicN));
        }
        return rhoVector.equals(derivedRhoVector) && (gcdTest.equals(BigInteger.ONE));
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONArray ja = new JSONArray();
        for (BigInteger item : sigmaVec) {
            ja.put(PosBigIntegerField.toBase64(item));
        }
        JSONObject m = new JSONObject();
        m.put("sigma_vec", ja);
        return m;
    }

    public static NiCorrectKeyProof fromJSONObject(JSONObject m) throws JSONException {
        JSONArray ja = m.getJSONArray("sigma_vec");
        List<BigInteger> sigmaVec = new ArrayList<>();
        for (int i = 0 ; i < ja.length(); i++) {
            String s = (String) ja.get(i);
            sigmaVec.add(PosBigIntegerField.fromBase64(s));
        }
        return new NiCorrectKeyProof(sigmaVec);
    }

}
