package com.silencelaboratories.twopartyecdsa.phe;


public class KeyPair {
    private final PaillierPrivateKey paillierPrivateKey;
    private final PaillierPublicKey paillierPublicKey;

    KeyPair(PaillierPrivateKey paillierPrivateKey, PaillierPublicKey paillierPublicKey) {
        this.paillierPrivateKey = paillierPrivateKey;
        this.paillierPublicKey = paillierPublicKey;
    }

    public PaillierPrivateKey getPrivateKey() {
        return paillierPrivateKey;
    }

    public PaillierPublicKey getPublicKey() {
        return paillierPublicKey;
    }

}
