package com.silencelaboratories.twopartyecdsa.signature;


import com.silencelaboratories.twopartyecdsa.exceptions.SerializerException;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.zkproofs.DLOGProof;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;


public class SignMessage3 {

    public final static String PHASE = "sign_message_3";
    private final String sessionId;
    private final ECPoint r1;
    private final DLOGProof dLogProof;
    private final BigInteger blindFactor;

    public SignMessage3(String sessionId, ECPoint r1, DLOGProof dlogProof, BigInteger blindFactor) {
        this.sessionId = sessionId;
        this.r1 = r1;
        this.dLogProof = dlogProof;
        this.blindFactor = blindFactor;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ECPoint getR1() {
        return r1;
    }

    public DLOGProof getdLogProof() {
        return dLogProof;
    }

    public BigInteger getBlindFactor() {
        return blindFactor;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("phase", PHASE);
        m.put("session_id", sessionId);
        m.put("r1", ECPointField.toHex(r1));
        m.put("dlog_proof", dLogProof.toJSONObject());
        m.put("blind_factor", PosBigIntegerField.toBase64(blindFactor));
        return m;
    }

    public static SignMessage3 fromJSONObject(JSONObject m) throws SerializerException {
        try {
            String phase = m.getString("phase");
            if (!phase.equals(SignMessage3.PHASE)) {
                throw new SerializerException("SignMessage3 invalid phase: " + phase);
            }
            String sessionId = m.getString("session_id");
            ECPoint r1 = ECPointField.fromHex(m.getString("r1"));
            DLOGProof dlogProof = DLOGProof.fromJSONObject(m.getJSONObject("dlog_proof"));
            BigInteger blindFactor = PosBigIntegerField.fromBase64(m.getString("blind_factor"));
            return new SignMessage3(sessionId, r1, dlogProof, blindFactor);

        } catch (JSONException | SerializerException e) {
            throw new SerializerException("SignMessage3 fromJSONObject() error", e);
        }
    }

}
