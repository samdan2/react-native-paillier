package com.silencelaboratories.twopartyecdsa.keygen;

public class KeyGenFailed extends Exception {
    public KeyGenFailed(String message) {
        super(message);
    }
    public KeyGenFailed(Exception e) {
        super(e);
    }
}
