package com.silencelaboratories.twopartyecdsa.zkproofs.pDLProof;

import static com.silencelaboratories.twopartyecdsa.utils.Utils.randomSampleBelow;

import com.silencelaboratories.twopartyecdsa.phe.EncryptedNumber;
import com.silencelaboratories.twopartyecdsa.serializers.ECPointField;
import com.silencelaboratories.twopartyecdsa.serializers.PosBigIntegerField;
import com.silencelaboratories.twopartyecdsa.serializers.StringField;
import com.silencelaboratories.twopartyecdsa.utils.CurveSecp256k1;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class PDLwSlackProof {
//    https://github.com/ZenGo-X/multi-party-ecdsa/blob/master/src/utilities/zk_pdl_with_slack/mod.rs
//    We use the proof as given in proof PIi in https://eprint.iacr.org/2016/013.pdf.
//    This proof ws taken from the proof 6.3 (left side ) in https://www.cs.unc.edu/~reiter/papers/2004/IJIS.pdf
//
//    Statement: (c, pk, Q, G)
//    witness (x, r) such that Q = xG, c = Enc(pk, x, r)
//    note that because of the range proof, the proof has a slack in the range: x in [-q^3, q^3]

    public static class PDLwSlackWitness {
        private final BigInteger x;
        private final BigInteger r;

        public PDLwSlackWitness(BigInteger x, BigInteger r) {
            this.x = x;
            this.r = r;
        }
    }

    private final BigInteger z;
    private final ECPoint u1;
    private final BigInteger u2;
    private final BigInteger u3;
    private final BigInteger s1;
    private final BigInteger s2;
    private final BigInteger s3;

    public PDLwSlackProof(BigInteger z, ECPoint u1, BigInteger u2, BigInteger u3,
                          BigInteger s1, BigInteger s2, BigInteger s3) {
        this.z = z;
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
    }

    private static BigInteger commitmentUnknownOrder(BigInteger h1, BigInteger h2, BigInteger nTilde,
                                                     BigInteger x, BigInteger r) {
        BigInteger h1_x = h1.modPow(x, nTilde);
        BigInteger h2_r;
        if (r.compareTo(BigInteger.ZERO) < 0) {
            BigInteger h2_inv = h2.modInverse(nTilde);
            h2_r = h2_inv.modPow(r.negate(), nTilde);
        } else {
            h2_r = h2.modPow(r, nTilde);
        }
        return h1_x.multiply(h2_r).mod(nTilde);
    }

    private static byte[] computeHash(
            ECPoint g, ECPoint q, EncryptedNumber ciphertext,
            BigInteger z, ECPoint u1, BigInteger u2, BigInteger u3, String sid, String pid
    ) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            ByteArrayOutputStream digestStream = new ByteArrayOutputStream();
            digestStream.write(ECPointField.toBytes(g));
            digestStream.write(ECPointField.toBytes(q));
            digestStream.write(PosBigIntegerField.toBytes(ciphertext.getCiphertext()));
            digestStream.write(PosBigIntegerField.toBytes(z));
            digestStream.write(ECPointField.toBytes(u1));
            digestStream.write(PosBigIntegerField.toBytes(u2));
            digestStream.write(PosBigIntegerField.toBytes(u3));
            digestStream.write(StringField.toBytes(sid));
            digestStream.write(StringField.toBytes(pid));
            return digest.digest(digestStream.toByteArray());
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static PDLwSlackProof prove(
            PDLwSlackWitness witness, PDLwSlackStatement statement, String sid, String pid
    ) {
        BigInteger q = CurveSecp256k1.order();
        ECPoint g = CurveSecp256k1.basePointG();

        BigInteger q3 = q.pow(3);
        BigInteger qNTilde = q.multiply(statement.getNTilde());
        BigInteger q3NTilde = q3.multiply(statement.getNTilde());

        BigInteger alpha = randomSampleBelow(q3);
        BigInteger beta = randomSampleBelow(statement.getEk().getN());
        BigInteger rho = randomSampleBelow(qNTilde);
        BigInteger gamma = randomSampleBelow(q3NTilde);

        BigInteger z = commitmentUnknownOrder(
                statement.getH1(),
                statement.getH2(),
                statement.getNTilde(),
                witness.x,
                rho
        );
        ECPoint u1 = g.multiply(alpha);
        BigInteger u2 = commitmentUnknownOrder(
                statement.getEk().getN().add(BigInteger.ONE),
                beta,
                statement.getEk().getNSquared(),
                alpha,
                statement.getEk().getN()
        );
        BigInteger u3 = commitmentUnknownOrder(
                statement.getH1(),
                statement.getH2(),
                statement.getNTilde(),
                alpha,
                gamma
        );
        byte[] hash = computeHash(
                statement.getG(), statement.getQ(), statement.getCiphertext(),
                z, u1, u2, u3, sid, pid);
        BigInteger e = PosBigIntegerField.fromBytes(hash);

        BigInteger s1 = e.multiply(witness.x).add(alpha);
        BigInteger s2 = commitmentUnknownOrder(
                witness.r,
                beta,
                statement.getEk().getN(),
                e,
                BigInteger.ONE
        );
        BigInteger s3 = e.multiply(rho).add(gamma);
        return new PDLwSlackProof(z, u1, u2, u3, s1, s2, s3);
    }

    public boolean verify(PDLwSlackStatement statement, String sid, String pid) {
        BigInteger q = CurveSecp256k1.order();
        ECPoint g = CurveSecp256k1.basePointG();

        byte[] hash = computeHash(statement.getG(), statement.getQ(), statement.getCiphertext(),
                z, u1, u2, u3, sid, pid);
        BigInteger e = PosBigIntegerField.fromBytes(hash);

        ECPoint g_s1 = g.multiply(s1);
        BigInteger e_fe_neg = q.subtract(e);
        ECPoint y_minus_e = statement.getQ().multiply(e_fe_neg);
        ECPoint u1Test = g_s1.add(y_minus_e);

        BigInteger u2TestTemp = commitmentUnknownOrder(
                statement.getEk().getN().add(BigInteger.ONE),
                s2,
                statement.getEk().getNSquared(),
                s1,
                statement.getEk().getN()
        );
        BigInteger u2Test = commitmentUnknownOrder(
                u2TestTemp,
                statement.getCiphertext().getCiphertext(),
                statement.getEk().getNSquared(),
                BigInteger.ONE,
                e.negate()
        );

        BigInteger u3TestTemp = commitmentUnknownOrder(
                statement.getH1(),
                statement.getH2(),
                statement.getNTilde(),
                s1,
                s3
        );
        BigInteger u3Test = commitmentUnknownOrder(
                u3TestTemp,
                z,
                statement.getNTilde(),
                BigInteger.ONE,
                e.negate()
        );

        return (u1.equals(u1Test) && u2.equals(u2Test) && u3.equals(u3Test));
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject m = new JSONObject();
        m.put("z", PosBigIntegerField.toBase64(z));
        m.put("u1", ECPointField.serialize(u1));
        m.put("u2", PosBigIntegerField.toBase64(u2));
        m.put("u3", PosBigIntegerField.toBase64(u3));
        m.put("s1", PosBigIntegerField.toBase64(s1));
        m.put("s2", PosBigIntegerField.toBase64(s2));
        m.put("s3", PosBigIntegerField.toBase64(s3));
        return m;
    }

    public static PDLwSlackProof fromJSONObject(JSONObject m) throws JSONException {
        BigInteger z = PosBigIntegerField.fromBase64((String) m.get("z"));
        ECPoint u1 = ECPointField.deserialize((String) m.get("u1"));
        BigInteger u2 = PosBigIntegerField.fromBase64((String) m.get("u2"));
        BigInteger u3 = PosBigIntegerField.fromBase64((String) m.get("u3"));
        BigInteger s1 = PosBigIntegerField.fromBase64((String) m.get("s1"));
        BigInteger s2 = PosBigIntegerField.fromBase64((String) m.get("s2"));
        BigInteger s3 = PosBigIntegerField.fromBase64((String) m.get("s3"));
        return new PDLwSlackProof(z, u1, u2, u3, s1, s2, s3);
    }

}
