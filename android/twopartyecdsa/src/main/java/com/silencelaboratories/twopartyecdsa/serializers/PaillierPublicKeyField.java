package com.silencelaboratories.twopartyecdsa.serializers;

import com.silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;

import java.math.BigInteger;

public class PaillierPublicKeyField {

    public static String serialize(PaillierPublicKey data) {
        BigInteger n = data.getN();
        return BytesBase64Field.serialize(PosBigIntegerField.toBytes(n));
    }

    public static PaillierPublicKey deserialize(String data) {
        return new PaillierPublicKey(PosBigIntegerField.fromBase64(data));
    }

}
