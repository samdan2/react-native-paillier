package com.silencelaboratories.twopartyecdsa.signature;

public class SignFailed extends Exception {
    public SignFailed(String message) {
        super(message);
    }
    public SignFailed(Exception e) {
        super(e);
    }
}
