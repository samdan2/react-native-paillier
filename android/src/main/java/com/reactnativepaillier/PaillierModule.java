package com.reactnativepaillier;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.module.annotations.ReactModule;
import com.silencelaboratories.twopartyecdsa.P1KeyShare;
import com.silencelaboratories.twopartyecdsa.keygen.KeyGenFailed;
import com.silencelaboratories.twopartyecdsa.keygen.KeyGenMessage1;
import com.silencelaboratories.twopartyecdsa.keygen.P1KeyGen;
import com.silencelaboratories.twopartyecdsa.keygen.P2KeyGen;

import org.json.JSONException;

import java.math.BigInteger;
import java.security.SecureRandom;

@ReactModule(name = PaillierModule.NAME)
public class PaillierModule extends ReactContextBaseJavaModule {
    public static final String NAME = "Paillier";

    public PaillierModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    public static byte[] randomBytes(int n) {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[n];
        random.nextBytes(bytes);
        return bytes;
    }



    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
    public void getRandomValues(int bitLength, Promise promise) throws KeyGenFailed, JSONException {
        String sessionId = "keygen_session_id";
        byte[] x1 = randomBytes(32);
        byte[] x2 = randomBytes(32);

        P1KeyGen p1KeyGen = new P1KeyGen(sessionId, x1);
        p1KeyGen.init();
        String p1KeyGenStorage = p1KeyGen.toJSONObject().toString();

        P2KeyGen p2KeyGen = new P2KeyGen(sessionId, x2);
        String p2KeyGenStorage= p2KeyGen.toJSONObject();

        P1KeyShare p1KeyShare = null;
        P2KeyShare p2KeyShare = null;
        boolean p1KeygenCompleted = false;
        boolean p2KeygenCompleted = false;

        JSONObject msgFromParty1;
        JSONObject msgFromParty2 = null;

        P1KeyGen tempP1KeyGen;
        P2KeyGen tempP2KeyGen;

        tempP1KeyGen = P1KeyGen.fromJSONObject(new JSONObject(p1KeyGenStorage));
        msgFromParty1 = tempP1KeyGen.getMessage1();
        p1KeyGenStorage = tempP1KeyGen.toJSONObject().toString();

        Log.d("Silentshard", msgFromParty1.toString());
        for (int i=0; i<2; i++) {
            if (!p2KeygenCompleted) {
                tempP2KeyGen = P2KeyGen.fromJSONObject(new JSONObject(p2KeyGenStorage));
                Pair<JSONObject, P2KeyShare> pair = tempP2KeyGen.processMessage(msgFromParty1);
                p2KeyGenStorage = tempP2KeyGen.toJSONObject().toString();
                if (pair.getFirst() != null) {
                    msgFromParty2 = pair.getFirst();
                    Log.d("Silentshard", msgFromParty2.toString());
                }
                if (pair.getSecond() != null) {
                    p2KeyShare = pair.getSecond();
                    p2KeygenCompleted = true;
                }
            }
            if (!p1KeygenCompleted) {
                tempP1KeyGen = P1KeyGen.fromJSONObject(new JSONObject(p1KeyGenStorage));
                Pair<JSONObject, P1KeyShare> pair = tempP1KeyGen.processMessage(msgFromParty2);
                p1KeyGenStorage = tempP1KeyGen.toJSONObject().toString();
                if (pair.getFirst() != null) {
                    msgFromParty1 = pair.getFirst();
                    Log.d("Silentshard", msgFromParty1.toString());
                }
                if (pair.getSecond() != null) {
                    p1KeyShare = pair.getSecond();
                    p1KeygenCompleted = true;
                }
            }
        }

        if ((p1KeyShare == null) || (p2KeyShare == null)) {
            throw new KeyGenFailed("p1KeyShare or p2KeyShare is null");
        }

        p1KeyShare = P1KeyShare.fromJSONObject(p1KeyShare.toJSONObject());
        p2KeyShare = P2KeyShare.fromJSONObject(p2KeyShare.toJSONObject());

        if (!Objects.equals(p1KeyShare.getPublicKeyHex(), p2KeyShare.getPublicKeyHex())) {
            throw new KeyGenFailed("Public keys not match");
        }
        BigInteger curveQ = CurveSecp256k1.order();
        BigInteger expectedPrivateKey = PosBigIntegerField.fromBytes(x1).multiply(PosBigIntegerField.fromBytes(x2)).mod(curveQ);
        BigInteger privateKey = p1KeyShare.getX1().multiply(p2KeyShare.getX2()).mod(curveQ);
        if (!privateKey.equals(expectedPrivateKey)) {
            throw new KeyGenFailed("Invalid private key");
        }
        return new Pair<>(p1KeyShare, p2KeyShare);


        promise.resolve();
    }
}
